variable "prefix" {
  default = "hn23-fr-nodejs-02-g2"
}

variable "project" {
  default = "hn23-fr-nodejs-02-g2"
}

variable "PROD_DB_USERNAME" {
  description = "Username for the RDS Postgres instance"
}

variable "PROD_DB_PASSWORD" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "mock-project-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "035280349565.dkr.ecr.us-east-1.amazonaws.com/hn23-fr-nodejs-02-g2:latest"
}
variable "JWT_SECRET" {
  description = "Secret key for the app"
}

variable "ACCESS_TOKEN_VALIDITY_SEC" {
  description = "Access token validity in seconds"
}

variable "TOKEN_ISSUER" {
  description = "Token issuer"
}

variable "PERPAGE" {
  description = "Page pagination default"
}
