[
    {
        "name": "api",
        "image": "${app_image}",
        "essential": true,
        "memoryReservation": 512,
        "environment": [
            {"name": "JWT_SECRET", "value": "${jwt_key}"},
            {"name": "PROD_DB_HOST", "value": "${db_host}"},
            {"name": "PROD_DB", "value": "${db_name}"},
            {"name": "PROD_DB_USERNAME", "value": "${db_user}"},
            {"name": "PROD_DB_PASSWORD", "value": "${db_pass}"},
            {"name": "ACCESS_TOKEN_VALIDITY_SEC", "value": "${access_token_validity}"},
            {"name": "TOKEN_ISSUER", "value": "${token_issuer}"},
            {"name": "PERPAGE", "value": "${per_page}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "api"
            }
        },
        "portMappings": [
            {
                "containerPort": 3000,
                "hostPort": 3000
            }
        ]
    }
]
