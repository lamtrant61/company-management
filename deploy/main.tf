terraform {
  backend "s3" {
    bucket         = "hn23-fr-nodejs-02-g2-devops-tfstate"
    key            = "hn23-fr-nodejs-02-g2-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "hn23-fr-nodejs-02-g2-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
