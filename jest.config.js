const path = require('path');

module.exports = {
  moduleNameMapper: {
    '^@src/(.*)$': path.join(__dirname, 'src/$1'),
  },
  modulePathIgnorePatterns: [
    './src/database/migrations',
    './src/database/seeders',
    './src/config',
    './src/server',
    './src/database/schema',
    './coverage',
    '.eslintrc.js',
    'jest.config.js',
  ],
  coverageDirectory: 'coverage',
  collectCoverage: true,
  collectCoverageFrom: ['**/*.js'],
  testEnvironment: 'node',
};
