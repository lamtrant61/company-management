FROM node:20.2.0

USER node

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY --chown=node:node . .

RUN npm install

ENV PORT=3000
ENV NODE_ENV=production

EXPOSE ${PORT}

CMD npx sequelize-cli db:migrate; npx sequelize-cli db:seed:all; yarn start:prod