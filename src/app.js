require('module-alias/register');
require('express-async-errors');
const express = require('express');
const officeRouter = require('@src/routers/office');
const employeeRouter = require('@src/routers/employee');
const customerRouter = require('@src/routers/customer');
const userRouter = require('@src/routers/Auth');
const productRouter = require('@src/routers/product');
const productlineRouter = require('@src/routers/productline');
const orderRouter = require('@src/routers/order');
const paymentRouter = require('@src/routers/payment');
const logRouter = require('@src/routers/log');
const { handleError } = require('@src/middlewares/handleError');

const docRoute = require('@src/routers/doc.route');
const { NotFoundError, ApiError, InternalError, ErrorType } = require('./core/ApiError');
// require('@src/logger/logger.connect');

const app = express();
app.use(express.json());

app.use('/api/v1/offices', officeRouter);
app.use('/api/v1/employees', employeeRouter);
app.use('/api/v1/customers', customerRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/products', productRouter);
app.use('/api/v1/product-lines', productlineRouter);
app.use('/api/v1/orders', orderRouter);
app.use('/api/v1/payments', paymentRouter);
app.use('/api/v1/document', docRoute);
app.use('/api/v1/logs', logRouter);

app.all('*', (req, res, next) => {
  next(new NotFoundError('Page not found'));
});

app.use(handleError);

module.exports = app;
