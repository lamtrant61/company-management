const { Product } = require('@src/database/models');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesProduct(req) {
  req.user ? 0 : (req.user = {});
}

const authorizationProductMiddleware = generateAuthorization('Products', prepareDataRulesProduct);

module.exports = {
  authorizationProductMiddleware,
  prepareDataRulesProduct,
};
