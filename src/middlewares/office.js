const {RoleCode} = require('@src/constant/roles')
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesOffices(req){
    req.user ? 0 : (req.user = {});
}

const authorizationOfficesMiddleware = generateAuthorization('Offices',prepareDataRulesOffices);

module.exports = {
    authorizationOfficesMiddleware,
    prepareDataRulesOffices
}