const { Customer } = require('@src/database/models');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesCustomer(req) {
  req.user ? 0 : (req.user = {});
  const customerId = await Customer.findOne({
    where: {
      userId: req.user.id,
    },
  });
  req.user.customerId = customerId;
}

const authorizationCustomerMiddleware = generateAuthorization(
  'Customers',
  prepareDataRulesCustomer,
);

module.exports = {
  authorizationCustomerMiddleware,
  prepareDataRulesCustomer,
};
