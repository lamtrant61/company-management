const { Employee, Customer } = require('@src/database/models');
const { RoleCode } = require('@src/constant/roles');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesPayment(req) {
  req.user ? 0 : (req.user = {});
  let personId = null;
  if (req.user.role !== RoleCode.CUSTOMER) {
    personId = await Employee.findOne({
      where: {
        userId: req.user.id,
      },
    });
    req.user.employeeId = personId;
  } else {
    personId = await Customer.findOne({
      where: {
        userId: req.user.id,
      },
    });
    req.user.customerId = personId;
  }
}

const authorizationPaymentMiddleware = generateAuthorization('Payments', prepareDataRulesPayment);

module.exports = {
  authorizationPaymentMiddleware,
  prepareDataRulesPayment,
};
