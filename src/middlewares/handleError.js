const { NotFoundError, ApiError, InternalError, ErrorType } = require('@src/core/ApiError');
const logger = require('@src/logger');
require('dotenv').config();

function handleError(err, req, res, next) {
  if (err instanceof ApiError) {
    ApiError.handle(err, res);

    if (err.type === ErrorType.INTERNAL) {
      logger.error.log(`500 - ${err.message} - ${req.originalUrl} - ${req.method}`);
    }
  } else {
    logger.error.log(`500 - ${err.message} - ${req.originalUrl} - ${req.method}`);
    logger.error.log(err);

    if (process.env.NODE_ENV === 'development') {
      return res.status(500).send(err);
    }

    ApiError.handle(new InternalError(), res);
  }
}

module.exports = {
  handleError,
};
