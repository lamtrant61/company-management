const { Product } = require('@src/database/models');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesProductLine(req) {
  req.user ? 0 : (req.user = {});
}

const authorizationProductlineMiddleware = generateAuthorization(
  'Productlines',
  prepareDataRulesProductLine,
);

module.exports = {
  authorizationProductlineMiddleware,
  prepareDataRulesProductLine,
};
