const JWT = require('@src/core/JWT');
const { User } = require('@src/database/models');
const asyncHandler = require('@src/utils/asyncHandler');
const { getAccessToken, validateTokenData } = require('@src/utils/authUtils');
const { AuthFailureError, TokenExpiredError, NotFoundError } = require('@src/core/ApiError');

module.exports = asyncHandler(async (req, res, next) => {
  req.accessToken = getAccessToken(req.headers.authorization);

  try {
    const payload = await JWT.validate(req.accessToken);
    validateTokenData(payload);

    const user = await User.findByPk(payload.sub);
    if (!user) throw new NotFoundError('User not registered');

    const role = await user.roleCode;

    user.role = role;
    req.user = user;
    next();
  } catch (e) {
    if (e instanceof TokenExpiredError) throw new TokenExpiredError();
    throw e;
  }
});
