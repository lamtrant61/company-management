const { Employee, Customer } = require('@src/database/models');
const { RoleCode } = require('@src/constant/roles');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesLog(req) {
  req.user ? 0 : (req.user = {});
}

const authorizationLoggerMiddleware = generateAuthorization('Logger', prepareDataRulesLog);

module.exports = {
  authorizationLoggerMiddleware,
  prepareDataRulesLog,
};
