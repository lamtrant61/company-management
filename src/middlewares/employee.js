const { Employee } = require('@src/database/models');
const { generateAuthorization } = require('./generateAuthorization');

async function prepareDataRulesEmployee(req) {
  req.user ? 0 : (req.user = {});
  const employeeId = await Employee.findOne({
    where: {
      userId: req.user.id,
    },
  });
  req.user.employeeId = employeeId;
}

const authorizationEmployeeMiddleware = generateAuthorization(
  'Employees',
  prepareDataRulesEmployee,
);

module.exports = {
  authorizationEmployeeMiddleware,
  prepareDataRulesEmployee,
};
