const { generateRules } = require('@src/constant/rules');
const asyncHandler = require('@src/utils/asyncHandler');
const { convertToBoolean, convertToWhereCondition } = require('@src/utils/handleRules');

function generateAuthorization(tableName, prepareDataRules) {
  return asyncHandler(async (req, res, next) => {
    await prepareDataRules(req);
    const rawRule = generateRules(req);
    const role = req.user.role;

    const rule = rawRule[role][tableName];
    for (const key in rule) {
      if (key === 'create') {
        rule[key] = convertToBoolean(rule[key]);
      } else {
        rule[key] = convertToWhereCondition(rule[key]);
      }
    }
    req.rule = rule;
    next();
  });
}

module.exports = {
  generateAuthorization,
};
