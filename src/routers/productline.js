const express = require('express');
const productlineController = require('@src/controllers/productline');
const authentication = require('@src/middlewares/authentication');
const { authorizationProductlineMiddleware } = require('@src/middlewares/productline');
const validate = require('@src/utils/validator');
const productLineSchema = require('@src/database/schema/productLine');

const router = express.Router();

router.use(authentication);
router.use(authorizationProductlineMiddleware);

router
  .route('/')
  .get(productlineController.getAllController())
  .post(validate(productLineSchema), productlineController.createController());

router
  .route('/:id')
  .get(productlineController.getByIdController())
  .put(validate(productLineSchema), productlineController.updateByIdController())
  .delete(productlineController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Product Line
 *   description: ProductLine management and retrieval
 */

/**
 * @swagger
 * /product-lines:
 *   get:
 *     summary: Get all product lines
 *     tags: [Product Line]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/ProductLine"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new product line
 *     tags: [Product Line]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ProductLine'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/ProductLine"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *
 * /product-lines/{id}:
 *   get:
 *     summary: Get a product line by ID
 *     tags: [Product Line]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product line ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/ProductLine"
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update a product line by ID
 *     tags: [Product Line]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product line ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ProductLine'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/ProductLine"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete a product line by ID
 *     tags: [Product Line]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product line ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
