const express = require('express');
const productController = require('@src/controllers/product');
const authentication = require('@src/middlewares/authentication');
const { authorizationProductMiddleware } = require('@src/middlewares/product');
const validate = require('@src/utils/validator');
const productSchema = require('@src/database/schema/product');

const router = express.Router();

router.use(authentication);
router.use(authorizationProductMiddleware);

router
  .route('/')
  .get(productController.getAllController())
  .post(validate(productSchema), productController.createController());

router
  .route('/:id')
  .get(productController.getByIdController())
  .put(validate(productSchema), productController.updateByIdController())
  .delete(productController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Product
 *   description: Product management and retrieval
 */

/**
 * @swagger
 * /products:
 *   get:
 *     summary: Get all products
 *     tags: [Product]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/Product"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new product
 *     tags: [Product]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Product'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Product"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *
 * /products/{id}:
 *   get:
 *     summary: Get a product by ID
 *     tags: [Product]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Product"
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update a product by ID
 *     tags: [Product]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Product'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Product"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete a product by ID
 *     tags: [Product]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Product ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
