const express = require('express');
const customerController = require('@src/controllers/customer');
const { authorizationCustomerMiddleware } = require('@src/middlewares/customer');
const { createCustomer, updateCustomer } = require('@src/database/schema/customer');
const validate = require('@src/utils/validator');

const authentication = require('@src/middlewares/authentication');
const router = express.Router();

router.use(authentication);
router.use(authorizationCustomerMiddleware);
router
  .route('/')
  .get(customerController.getAllController())
  .post(validate(createCustomer), customerController.createController());

router
  .route('/:id')
  .get(customerController.getByIdController())
  .put(validate(updateCustomer), customerController.updateByIdController())
  .delete(customerController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Customer
 *   description: Customer management and retrieval
 */

/**
 * @swagger
 * /customers:
 *   get:
 *     summary: Get all customers
 *     tags: [Customer]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/NewCustomer"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new customer
 *     tags: [Customer]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewCustomer'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/NewCustomer"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *
 * /customers/{id}:
 *   get:
 *     summary: Get a customer by ID
 *     tags: [Customer]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Customer ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/NewCustomer"
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update a customer by ID
 *     tags: [Customer]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Customer ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateCustomer'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/UpdateCustomer"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete a customer by ID
 *     tags: [Customer]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Customer ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
