const express = require('express');
const employeeController = require('@src/controllers/employee');
const { authorizationEmployeeMiddleware } = require('@src/middlewares/employee');
const authentication = require('@src/middlewares/authentication');

const router = express.Router();
const { createEmployee, updateEmployeee } = require('@src/database/schema/employee');
const validate = require('@src/utils/validator');

router.use(authentication);
router.use(authorizationEmployeeMiddleware);

router
  .route('/')
  .get(employeeController.getAllController())
  .post(validate(createEmployee), employeeController.createController());

router
  .route('/:id')
  .get(employeeController.getByIdController())
  .put(validate(updateEmployeee), employeeController.updateByIdController())
  .delete(employeeController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Employee
 *   description: Employee management and retrieval
 */

/**
 * @swagger
 * /employees:
 *   get:
 *     summary: Get all employees
 *     tags: [Employee]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/Employee"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new employees
 *     tags: [Employee]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewEmployee'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/NewEmployee"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *
 * /employees/{id}:
 *   get:
 *     summary: Get a employee by ID
 *     tags: [Employee]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Employee ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Employee"
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update a employee by ID
 *     tags: [Employee]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Employee ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateEmployee'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/UpdateEmployee"
 *       400:
 *         description: Bad Request
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete a employee by ID
 *     tags: [Employee]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Employee ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *       403:
 *         description: Forbidden
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
