const express = require('express');
const paymentController = require('@src/controllers/payment');
const authentication = require('@src/middlewares/authentication');
const { authorizationPaymentMiddleware } = require('@src/middlewares/payment');

const router = express.Router();

router.use(authentication);
router.use(authorizationPaymentMiddleware);

router
  .route('/')
  .get(paymentController.getAllController())
  .post(paymentController.createController());

router
  .route('/:id')
  .get(paymentController.getByIdController())
  .put(paymentController.updateByIdController())
  .delete(paymentController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Payment
 *   description: Payment management and retrieval
 */

/**
 * @swagger
 *  /payments:
 *    get:
 *      summary: Get all payment
 *      tags: [Payment]
 *      security:
 *        - Bearer: []
 *      responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 */

