const express = require('express');
const officeController = require('../controllers/office');
const validate = require('@src/utils/validator');
const schema = require('@src/database/schema/office');
const authentication = require('@src/middlewares/authentication');
const {authorizationOfficesMiddleware} = require('@src/middlewares/office');

const router = express.Router();

router.use(authentication);
router.use(authorizationOfficesMiddleware);


router
  .route('/')
  .get(officeController.getAllController())
  .post(validate(schema),officeController.createController());

router
  .route('/:id')
  .get(officeController.getByIdController())
  .put(validate(schema),officeController.updateByIdController())
  .delete(officeController.deleteByIdController());

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Office
 *   description: Office management and retrieval
 */


/**
 * @swagger
 * /offices:
 *   get:
 *     summary: Get all office
 *     tags: [Office]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/Office"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new office 
 *     tags: [Office]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: "#/components/schemas/Office"
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Office"
 *       400:
 *         description: Bad Request
 *       500:
 *         description: Internal Server Error
 *      
 *
 * /offices/{id}:
 *   get:
 *     summary: Get an office by ID
 *     tags: [Office]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: office ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Office"
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update an office by ID
 *     tags: [Office]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Office ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: "#/components/schemas/Office"
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Office"
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete an office by ID
 *     tags: [Office]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: office ID
 *         required: true
 *         schema:
 *          type: string
 *     responses:
 *       200:
 *         description: Success
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */

