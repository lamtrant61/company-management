const express = require('express');
const orderController = require('@src/controllers/order');
const authentication = require('@src/middlewares/authentication');
const {authorizationOrderMiddleware} = require('@src/middlewares/order');
const {createOrder , updateOrder} = require('@src/database/schema/order');
const validate = require('@src/utils/validator');
const router = express.Router();

router.use(authentication);
router.use(authorizationOrderMiddleware);

router
  .route('/')
  .get(orderController.getAllController())
  .post(validate(createOrder),orderController.createController());

router
  .route('/:id')
  .get(orderController.getByIdController())
  .put(validate(updateOrder),orderController.updateByIdController())
  .delete(orderController.deleteByIdController());

module.exports = router;


/**
 * @swagger
 * tags:
 *   name: Order
 *   description: Order management and retrieval
 */


/**
 * @swagger
 * /orders:
 *   get:
 *     summary: Get all orders
 *     tags: [Order]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/OrderResponse"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *   post:
 *     summary: Create a new order 
 *     tags: [Order]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: "#/components/schemas/NewOrder"
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/OrderResponse"
 *       400:
 *         description: Bad Request
 *       500:
 *         description: Internal Server Error
 *      
 *
 * /orders/{id}:
 *   get:
 *     summary: Get an order by ID
 *     tags: [Order]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Order ID
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/OrderResponse"
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   put:
 *     summary: Update an order by ID
 *     description: The customer cannot update the order, you need to log in to another account, or enter a different token to perform the action.
 *     tags: [Order]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Order ID
 *         required: true
 *         type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: "#/components/schemas/UpdateOrder"
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/OrderResponse"
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete an order by ID
 *     tags: [Order]
 *     security:
 *       - Bearer: []
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Order ID
 *         required: true
 *         schema:
 *          type: string
 *       - name: comments
 *         in: query
 *         description: Comments for deleting the order
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Success
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
