const express = require('express');
const logController = require('@src/controllers/log');
const { authorizationLoggerMiddleware } = require('@src/middlewares/log');

const authentication = require('@src/middlewares/authentication');
const router = express.Router();

router.use(authentication);
router.use(authorizationLoggerMiddleware);
router
  .route('/')
  .get(logController.getAllController())
  .put(logController.updateLevelController())
  .delete(logController.deleteController());

module.exports = router;


/**
 * @swagger
 * tags:
 *   name: Log
 *   description: Log management and retrieval
 */


/**
 * @swagger
 * /logs:
 *   get:
 *     summary: Get all Log
 *     tags: [Log]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/components/schemas/Log"
 *       403:
 *         description: Forbidden
 *       500:
 *         description: Internal Server Error
 *      
 *   put:
 *     summary: Update Log
 *     tags: [Log]
 *     security:
 *       - Bearer: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: "#/components/schemas/UpdateLog"
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: "#/components/schemas/Log"
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 *   delete:
 *     summary: Delete all log
 *     tags: [Log]
 *     security:
 *       - Bearer: []
 *     responses:
 *       200:
 *         description: Success
 *       404:
 *         description: Not Found
 *       500:
 *         description: Internal Server Error
 */
