const express = require('express');
const authController = require('@src/controllers/auth.controller');
const router = express.Router();
const { signIn, signUp } = require('@src/database/schema/user');
const validator = require('@src/utils/validator');

router.post('/signin', validator(signIn), authController.signin);
router.post('/signup', validator(signUp), authController.signup);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: User management and retrieval
 */

/**
 * @swagger
 * paths:
 *   /users/signup:
 *     post:
 *       tags:
 *         - Users
 *       summary: Register a new user
 *       requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserSignUp'
 *       responses:
 *         '201':
 *           description: User registered
 *         '400':
 *           description: Bad request
 *
 *   /users/signin:
 *     post:
 *       tags:
 *         - Users
 *       summary: Log in as a user
 *       requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserSignIn'
 *       responses:
 *         '200':
 *           description: User logged in
 *         '401':
 *           description: Unauthorized
 *         '400':
 *           description: Bad request
 */
