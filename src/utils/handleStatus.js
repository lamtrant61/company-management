const { OrderStatus, OrderRule } = require('@src/constant/status');

/**
 * Checks if a given order status can be updated to a specified status based on predefined rules.
 *
 * @param {string} currentStatus - The current status of the order.
 * @param {string} updateStatus - The desired status to update the order to.
 * @returns {boolean} - Returns true if the update is allowed based on the rules; otherwise, false.
 */
function handleOrderStatus(currentStatus, updateStatus) {
  const updateStatusKey = Object.keys(OrderStatus).filter(
    key => OrderStatus[key] === updateStatus,
  )[0];
  return OrderRule[updateStatusKey].includes(currentStatus);
}

module.exports = handleOrderStatus;
