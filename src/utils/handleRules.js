const { Op } = require('sequelize');

/**
 * Merge two SQL WHERE conditions
 * @param {object} condition1 - The first WHERE condition in Sequelize
 * @param {object} condition2 - The second WHERE condition in Sequelize
 * @param {string} operator - The operator to merge the two conditions ('and' or 'or')
 * @returns {object} - The merged SQL WHERE condition
 */
function mergeConditions(condition1, condition2, operator) {
  let result = {
    [Op[operator]]: [condition1, condition2],
  };
  return result;
}

/**
 * Convert an array to an object for WHERE condition in Sequelize
 * @param {Array} arr - The array representing the WHERE condition in Sequelize
 * @returns {object} - The WHERE condition in Sequelize object
 */
function arrayToWhereCondition(arr) {
  const [field, comparison, value] = arr;
  let result = {
    [field]: {
      [Op[comparison]]: value,
    },
  };
  return result;
}

/**
 * Convert an array to a boolean value based on the comparison operator
 * @param {Array} arr - The array representing the condition
 * @returns {boolean} - The converted boolean value
 */
function arrayToBoolean(arr) {
  const [field, comparison, value] = arr;
  let result = false;
  switch (comparison) {
    case 'eq': // Equal to
      result = field == value;
      break;
    case 'gt': // Greater than
      result = field > value;
      break;
    case 'gte': // Greater than or equal to
      result = field >= value;
      break;
    case 'ne': // Not equal to
      result = field != value;
      break;
    case 'is': // IS NULL
      result = field === value;
      break;
    case 'not': // IS NOT TRUE
      result = field !== value;
      break;
    case 'or': // OR condition
      result = value.includes(field);
      break;
    case 'lt': // Less than
      result = field < value;
      break;
    case 'lte': // Less than or equal to
      result = field <= value;
      break;
    case 'between': // Between
      result = field >= value[0] && field <= value[1];
      break;
    case 'notBetween': // Not between
      result = field < value[0] || field > value[1];
      break;
    case 'in': // IN
      result = value.includes(field);
      break;
    case 'notIn': // NOT IN
      result = !value.includes(field);
      break;
    case 'like': // LIKE
      result = field.includes(value);
      break;
    case 'notLike': // NOT LIKE
      result = !field.includes(value);
      break;
    case 'startsWith': // LIKE 'value%'
      result = field.startsWith(value);
      break;
    case 'endsWith': // LIKE '%value'
      result = field.endsWith(value);
      break;
    case 'substring': // LIKE '%value%'
      result = field.includes(value);
      break;
    case 'iLike': // ILIKE (case insensitive)
      result = field.toLowerCase().includes(value.toLowerCase());
      break;
    case 'notILike': // NOT ILIKE (case insensitive)
      result = !field.toLowerCase().includes(value.toLowerCase());
      break;
    case 'any': // ANY
      result = value.includes(field);
      break;
    default:
      break;
  }

  return !!result;
}

/**
 * Merge two boolean values based on the operator
 * @param {boolean} condition1 - The first boolean condition
 * @param {boolean} condition2 - The second boolean condition
 * @param {string} operator - The operator to merge the conditions ('and' or 'or')
 * @returns {boolean} - The merged boolean condition
 */
function mergeBooleans(condition1, condition2, operator) {
  let result = null;
  if (operator === 'or') {
    result = condition1 || condition2;
  } else if (operator === 'and') {
    result = condition1 && condition2;
  }

  return result;
}

/**
 * Convert an array of conditions to a rule object
 * @param {Array} conditions - The array of conditions
 * @param {function} handleChangeCondition - The function to handle converting a condition array to an object
 * @param {function} handleMergeCondition - The function to handle merging two conditions
 * @returns {object|undefined} - The rule object, or undefined if the input is invalid
 */
function convertToRule(conditions, handleChangeCondition, handleMergeCondition) {
  if (conditions.length === 0) {
    return {};
  }
  const operators = [];
  const whereConditions = [];
  const result = [];

  for (let i = conditions.length - 1; i >= 0; i--) {
    // push the condition or operator to there own stack
    if (typeof conditions[i] === 'object') {
      whereConditions.push(handleChangeCondition(conditions[i]));
    } else {
      operators.push(conditions[i]);
    }

    // pop the condition or operator to merge to where condition use in sequelize
    if (whereConditions.length >= 2 && operators.length >= 1) {
      const condition1 = whereConditions.pop();
      const condition2 = whereConditions.pop();
      const operator = operators.pop();
      whereConditions.push(handleMergeCondition(condition1, condition2, operator));
    }
  }

  // last check if the user make valid rule
  if (result.length === 0 && whereConditions.length === 1) {
    return whereConditions[0];
  }
  return;
}

/**
 * Standardize the conditions by replacing the field values with corresponding data values
 * @param {Array} conditions - The array of conditions
 * @param {object} data - The data object containing field-value mappings
 * @returns {Array} - The standardized conditions array
 */
function standardizeConditions(conditions, data) {
  const cloneConditions = [...conditions];
  cloneConditions.forEach(element => {
    if (typeof element === 'object') {
      element[0] = data[element[0]];
    }
  });

  return cloneConditions;
}

/**
 * Convert an array of conditions to a string for SQL WHERE condition
 * @param {Array} conditions - The array of conditions
 * @returns {object|undefined} - The SQL WHERE condition string, or undefined if the input is invalid
 */
function convertToWhereCondition(conditions) {
  return convertToRule(conditions, arrayToWhereCondition, mergeConditions);
}

/**
 * Convert an array of conditions to a boolean value
 * @param {Array} conditions - The array of conditions
 * @param {object} data - The data object containing field-value mappings
 * @returns {boolean} - The converted boolean value
 */
function convertToBoolean(conditions, data) {
  const myConditions = standardizeConditions(conditions, data);
  return convertToRule(myConditions, arrayToBoolean, mergeBooleans);
}

module.exports = {
  arrayToBoolean,
  arrayToWhereCondition,
  convertToBoolean,
  convertToRule,
  convertToWhereCondition,
  mergeBooleans,
  mergeConditions,
  standardizeConditions,
};
