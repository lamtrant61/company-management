const _ = require('lodash');

exports.getUserData = async user => {
  const data = _.pick(user, ['id', 'username', 'email', 'roleId']);
  return data;
};
