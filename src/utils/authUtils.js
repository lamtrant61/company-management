const { AuthFailureError, InternalError } = require('@src/core/ApiError');
const JWT = require('@src/core/JWT');
const { JwtPayLoad } = require('@src/core/JWT');
require('dotenv').config();

const tokenInfo = {
  accessTokenValidity: parseInt(process.env.ACCESS_TOKEN_VALIDITY_SEC || '0'),
  issuer: process.env.TOKEN_ISSUER || '',
};

function getAccessToken(authorization) {
  if (!authorization) throw new AuthFailureError('Invalid Authorization');
  if (!authorization.startsWith('Bearer ')) throw new AuthFailureError('Invalid Authorization');
  return authorization.split(' ')[1];
}

function validateTokenData(payload) {
  if (!payload || !payload.iss || !payload.sub || payload.iss !== tokenInfo.issuer)
    throw new AuthFailureError('Invalid Token Data');
  return true;
}

async function createToken(user) {
  if (!process.env.JWT_SECRET) throw new InternalError('Token generation failure');
  const accessToken = await JWT.encode(
    new JwtPayLoad(tokenInfo.issuer, user.id, tokenInfo.accessTokenValidity),
  );
  return accessToken;
}

module.exports = {
  getAccessToken,
  validateTokenData,
  createToken,
};
