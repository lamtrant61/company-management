const { Query } = require('@src/constant/query');
const { Op } = require('sequelize');
require('dotenv').config();

function getQuery({ query }) {
  const result = {
    pages: 0,
    perpages: process.env.PERPAGE,
  };

  for (let i = 0; i < Query.length; i++) {
    if (query[Query[i]]) {
      result[Query[i]] = query[Query[i]];
    }
  }

  return result;
}

function generatePaginationCondition({ pages, perpages }) {
  const result = {};
  if (pages && pages > 0 && perpages) {
    result.limit = Number(perpages);
    result.offset = (Number(pages) - 1) * Number(perpages);
  }

  return result;
}

module.exports = {
  getQuery,
  generatePaginationCondition,
};
