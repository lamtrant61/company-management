const asyncHandler = require('@src/utils/asyncHandler');
const { Op } = require('sequelize');
const { NotFoundError } = require('@src/core/ApiError');
const { ForbiddenError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const { generatePaginationCondition, getQuery } = require('@src/utils/handleQuery');
const logger = require('@src/logger');
class ControllerFactory {
  Model;
  constructor(Model) {
    this.Model = Model;
  }

  createController() {
    return asyncHandler(async (req, res, next) => {
      req.rule ? 0 : (req.rule = {});
      if (!req.rule['create']) {
        logger.warning.log(
          `Permission denied create to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        return next(new ForbiddenError('Permission denied create'));
      }
      const createdObject = await this.Model.create(req.body);

      logger.info.log(`Create success to table ${this.Model.getTableName()}`, req.user.email);

      return new SuccessResponse('Create success', createdObject).send(res);
    });
  }

  getAllController() {
    return asyncHandler(async (req, res, next) => {
      req.rule ? 0 : (req.rule = {});
      if (!req.rule['read']) {
        logger.warning.log(
          `Permission denied read to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        return next(new ForbiddenError('Permission denied read'));
      }
      let allData = null;

      const query = getQuery(req);
      const paginationCondition = generatePaginationCondition(query);

      allData = await this.Model.findAll({
        where: req.rule['read'] || {},
        ...paginationCondition,
      });

      logger.info.log(
        `Read success to table ${this.Model.getTableName()}`,
        req.user.dataValues.email,
      );

      return new SuccessResponse('Success', allData).send(res);
    });
  }

  getByIdController() {
    return asyncHandler(async (req, res, next) => {
      req.rule ? 0 : (req.rule = {});
      if (!req.rule['read']) {
        logger.warning.log(
          `Permission denied read to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        return next(new ForbiddenError('Permission denied read'));
      }
      const { id } = req.params;
      const data = await this.Model.findOne({
        where: {
          [Op.and]: [
            {
              id,
            },
            req.rule['read'] || {},
          ],
        },
      });
      if (!data) {
        logger.warning.log(
          `Permission denied read to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        next(new NotFoundError('fail not found'));
      }
      logger.info.log(`Read success to table ${this.Model.getTableName()}`, req.user.email);
      return new SuccessResponse('Success', data).send(res);
    });
  }

  updateByIdController() {
    return asyncHandler(async (req, res, next) => {
      req.rule ? 0 : (req.rule = {});
      if (!req.rule['update']) {
        logger.warning.log(
          `Permission denied create to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        return next(new ForbiddenError('Permission denied update'));
      }
      const { id } = req.params;
      const data = await this.Model.findOne({
        where: {
          [Op.and]: [
            {
              id,
            },
            req.rule['update'] || {},
          ],
        },
      });
      if (!data) {
        logger.warning.log(`fail not found`, req.user.email);
        next(new NotFoundError('fail not found'));
      }
      for (const key in req.body) {
        data[key] = req.body[key];
      }
      await data.save();
      logger.info.log(`Update success to table ${this.Model.getTableName()}`, req.user.email);
      return new SuccessResponse('Success', data).send(res);
    });
  }

  deleteByIdController() {
    return asyncHandler(async (req, res, next) => {
      req.rule ? 0 : (req.rule = {});
      if (!req.rule['delete']) {
        logger.warning.log(
          `Permission denied create to table ${this.Model.getTableName()}`,
          req.user.email,
        );
        return next(new ForbiddenError('Permission denied delete'));
      }
      const { id } = req.params;

      const data = await this.Model.findOne({
        where: {
          [Op.and]: [
            {
              id,
            },
            req.rule['delete'] || {},
          ],
        },
      });
      if (!data) {
        logger.warning.log(`fail not found`, req.user.email);
        next(new NotFoundError('fail not found'));
      }

      await data.destroy();
      logger.info.log(`Delete success to table ${this.Model.getTableName()}`, req.user.email);
      return new SuccessMsgResponse('Success').send(res);
    });
  }
}

module.exports = { ControllerFactory };
