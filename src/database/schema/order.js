const Joi = require('joi');

const createOrder = Joi.object({
  comments: Joi.string().required(),
  orderDetails: Joi.array()
    .items(
      Joi.object({
        productCode: Joi.string().required(),
        quantityOrdered: Joi.number().integer().min(1).required(),
      }),
    )
    .required(),
});

const updateOrder = Joi.object({
  comments: Joi.string().required(),
  status: Joi.string()
    .valid('On Hold', 'In Progress', 'Disputed', 'Resolved', 'Shipped', 'Cancelled')
    .required(),
});

module.exports = {createOrder , updateOrder};
