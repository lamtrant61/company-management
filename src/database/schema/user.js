const Joi = require('joi');

const signUp = Joi.object().keys({
  username: Joi.string().min(3).max(25).required(),
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).max(100).required(),
});

const signIn = Joi.object().keys({
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).max(100).required(),
});

module.exports = { signIn, signUp };
