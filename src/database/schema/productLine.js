const Joi = require('joi');

const productLineSchema = Joi.object({
  productLine: Joi.string().required(),
  textDescription: Joi.string().required(),
  htmlDescription: Joi.string().required(),
  image: Joi.string().required(),
});

module.exports = productLineSchema;
