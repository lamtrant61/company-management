const Joi = require('joi');

const createEmployee = Joi.object({
  username: Joi.string().min(3).max(30).required(),
  password: Joi.string().min(3).max(30),
  lastName: Joi.string().min(3).max(30).required(),
  firstName: Joi.string().min(3).max(30).required(),
  roleId: Joi.number(),
  extension: Joi.string(),
  email: Joi.string().email().required(),
  userId: Joi.number().integer(),
  officeId: Joi.number().integer().required(),
  reportsTo: Joi.number().allow(null),
  jobTitle: Joi.string().min(3).max(30).required(),
});

const updateEmployeee = Joi.object({
  lastName: Joi.string().min(3).max(30).required(),
  firstName: Joi.string().min(3).max(30).required(),
  roleId: Joi.number(),
  extension: Joi.string(),
  officeId: Joi.number().integer().required(),
  reportsTo: Joi.number().allow(null),
  jobTitle: Joi.string().min(3).max(30).required(),
});

module.exports = { createEmployee, updateEmployeee };
