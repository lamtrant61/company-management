const Joi = require('joi');

const createCustomer = Joi.object({
  username: Joi.string().min(3).max(25).required(),
  email: Joi.string().min(6).required().email(),
  password: Joi.string()
    .min(6)
    .max(100)
    .regex(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,}$/)
    .required(),
  customerName: Joi.string().min(3).max(30).required(),
  userId: Joi.number().integer(),
  contactLastName: Joi.string().min(3).max(30).required(),
  contactFirstName: Joi.string().min(3).max(30).required(),
  phone: Joi.string().min(6).max(30).required(),
  addressLine1: Joi.string().min(5).required(),
  addressLine2: Joi.string().min(5).allow(null),
  city: Joi.string().min(3).max(50).required(),
  state: Joi.string().min(3).max(50).required(),
  postalCode: Joi.string().min(2).max(20).required(),
  country: Joi.string().min(2).max(30).required(),
  salesRepEmployeeNumber: Joi.number().integer(),
  creditLimit: Joi.number().integer().required(),
});

const updateCustomer = Joi.object({
  customerName: Joi.string().min(3).max(30).required(),
  userId: Joi.number().integer(),
  contactLastName: Joi.string().min(3).max(30).required(),
  contactFirstName: Joi.string().min(3).max(30).required(),
  phone: Joi.string().min(6).max(30).required(),
  addressLine1: Joi.string().min(5).required(),
  addressLine2: Joi.string().min(5).allow(null),
  city: Joi.string().min(3).max(50).required(),
  state: Joi.string().min(3).max(50).allow(null).required(),
  postalCode: Joi.string().min(2).max(20).required(),
  country: Joi.string().min(2).max(30).required(),
  salesRepEmployeeNumber: Joi.number().integer(),
  creditLimit: Joi.number().integer().required(),
});

module.exports = { createCustomer, updateCustomer };
