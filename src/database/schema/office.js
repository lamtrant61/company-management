const Joi = require('joi');

const officeSchema = Joi.object({
  officeCode: Joi.number().integer().required(),
  city: Joi.string().min(3).max(20).required(),
  phone: Joi.string().min(3).max(30).required(),
  addressLine1: Joi.string().min(3).max(50).required(),
  addressLine2: Joi.string(),
  state: Joi.string().min(3).max(30).required(),
  country: Joi.string().min(2).max(30).required(),
  postalCode: Joi.string().required(),
  territory: Joi.string().required(),
});

module.exports = officeSchema;
