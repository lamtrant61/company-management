const Joi = require('joi');

const productSchema = Joi.object({
  productCode: Joi.string().required(),
  productName: Joi.string().required(),
  productLine: Joi.string().required(),
  productScale: Joi.string().required(),
  productVendor: Joi.string().required(),
  quantityInStock: Joi.number().integer().required(),
  buyPrice: Joi.number().required(),
  MSRP: Joi.number().required(),
});

module.exports = productSchema;
