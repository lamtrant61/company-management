'use strict';
const { Model } = require('sequelize');
const PROTECTED_ATTRIBUTES = ['createdAt', 'updatedAt'];

module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    toJSON() {
      // hide protected fields
      let attributes = Object.assign({}, this.get());
      for (let a of PROTECTED_ATTRIBUTES) {
        delete attributes[a];
      }
      return attributes;
    }

    static associate(models) {
      Payment.belongsTo(models.Order, {
        foreignKey: 'orderId',
      });
      Payment.belongsTo(models.Customer, {
        foreignKey: 'customerId',
      });
      models.Order.hasOne(Payment, {
        foreignKey: 'orderId',
      });
    }
  }
  Payment.init(
    {
      customerId: DataTypes.INTEGER,
      orderId: DataTypes.INTEGER,
      checkNumber: DataTypes.INTEGER,
      paymentDate: DataTypes.DATE,
      amount: DataTypes.INTEGER,
      delete: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'Payment',
    },
  );
  return Payment;
};
