'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Orderdetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Orderdetail.belongsTo(models.Order, {
        foreignKey: 'orderId',
      });
      Orderdetail.belongsTo(models.Product, {
        foreignKey: 'productCode',
      });
      models.Order.hasMany(Orderdetail, {
        foreignKey: 'orderId',
      });
      models.Product.hasMany(Orderdetail, {
        foreignKey: 'productCode',
      });
    }
  }
  Orderdetail.init(
    {
      orderId: DataTypes.INTEGER,
      productCode: DataTypes.STRING,
      quantityOrdered: DataTypes.INTEGER,
      priceEach: DataTypes.DECIMAL(10, 2),
      orderLineNumber: DataTypes.INTEGER,
      delete: DataTypes.BOOLEAN,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'Orderdetail',
    },
  );
  return Orderdetail;
};
