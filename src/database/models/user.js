'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsTo(models.Role, {
        foreignKey: 'roleId',
        as: 'role',
      });
    }
  }
  User.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      roleId: DataTypes.INTEGER,
      roleCode: {
        type: DataTypes.VIRTUAL,
        async get() {
          const role = await sequelize.models.Role.findOne({ where: { id: this.roleId } });
          return role ? role.code : null;
        },
        set(value) {
          throw new Error('Can not set value for this field');
        },
      },
    },
    {
      sequelize,
      modelName: 'User',
    },
  );
  return User;
};
