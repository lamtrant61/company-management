'use strict';
const { Model } = require('sequelize');
const PROTECTED_ATTRIBUTES = ['createdAt', 'updatedAt'];

module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    toJSON() {
      // hide protected fields
      let attributes = Object.assign({}, this.get());
      for (let a of PROTECTED_ATTRIBUTES) {
        delete attributes[a];
      }
      return attributes;
    }

    static associate(models) {
      Product.belongsTo(models.Productline, {
        foreignKey: 'productLine',
        as: 'line',
      });
    }
  }
  Product.init(
    {
      productCode: DataTypes.STRING,
      productName: DataTypes.STRING,
      productLine: DataTypes.STRING,
      productScale: DataTypes.STRING,
      productVendor: DataTypes.STRING,
      quantityInStock: DataTypes.INTEGER,
      buyPrice: DataTypes.DECIMAL(10, 2),
      MSRP: DataTypes.DECIMAL(10, 2),
    },
    {
      sequelize,
      modelName: 'Product',
    },
  );
  return Product;
};
