'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Order.belongsTo(models.Customer, {
        foreignKey: 'customerId',
        as: 'customer',
      });
    }
  }
  Order.init(
    {
      orderDate: DataTypes.DATE,
      requiredDate: DataTypes.DATE,
      shippedDate: DataTypes.DATE,
      status: DataTypes.STRING,
      comments: DataTypes.STRING,
      customerId: DataTypes.INTEGER,
      delete: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'Order',
    },
  );
  return Order;
};
