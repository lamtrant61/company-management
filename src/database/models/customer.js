'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Customer.belongsTo(models.Employee, {
        foreignKey: 'salesRepEmployeeNumber',
        as: 'employee',
      });

      Customer.hasMany(models.Order, {
        foreignKey: 'customerId',
        as: 'orders',
      });

      Customer.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user',
      });
    }
  }
  Customer.init(
    {
      customerName: DataTypes.STRING,
      userId: DataTypes.INTEGER,
      contactLastName: DataTypes.STRING,
      contactFirstName: DataTypes.STRING,
      phone: DataTypes.STRING,
      addressLine1: DataTypes.STRING,
      addressLine2: DataTypes.STRING,
      city: DataTypes.STRING,
      state: DataTypes.STRING,
      postalCode: DataTypes.STRING,
      country: DataTypes.STRING,
      salesRepEmployeeNumber: DataTypes.INTEGER,
      creditLimit: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'Customer',
    },
  );
  return Customer;
};
