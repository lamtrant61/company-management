'use strict';
const { Model } = require('sequelize');
const PROTECTED_ATTRIBUTES = ['createdAt', 'updatedAt'];

module.exports = (sequelize, DataTypes) => {
  class Productline extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    toJSON() {
      // hide protected fields
      let attributes = Object.assign({}, this.get());
      for (let a of PROTECTED_ATTRIBUTES) {
        delete attributes[a];
      }
      return attributes;
    }

    static associate(models) {
      Productline.hasMany(models.Product, {
        foreignKey: 'productLine',
        as: 'line',
      });
    }
  }
  Productline.init(
    {
      productLine: DataTypes.STRING,
      textDescription: DataTypes.STRING,
      htmlDescription: DataTypes.STRING,
      image: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Productline',
    },
  );
  return Productline;
};
