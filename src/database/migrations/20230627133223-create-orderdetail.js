'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Orderdetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      orderId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Orders',
          key: 'id',
        },
        onUpdate: 'cascade',
      },
      productCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Products',
          key: 'productCode',
        },
        onUpdate: 'cascade',
      },
      quantityOrdered: {
        type: Sequelize.INTEGER, // Number of products ordered
      },
      priceEach: {
        type: Sequelize.DECIMAL(10, 2),
      },
      orderLineNumber: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      delete: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Orderdetails');
  },
};
