'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      productCode: {
        type: Sequelize.STRING,
        unique: true,
      },
      productName: {
        type: Sequelize.STRING,
      },
      productLine: {
        type: Sequelize.STRING,
        references: {
          model: 'Productlines',
          key: 'productLine',
        },
        onUpdate: 'cascade',
      },
      productScale: {
        type: Sequelize.STRING,
      },
      productVendor: {
        type: Sequelize.STRING, //Company that produce this product
      },
      quantityInStock: {
        type: Sequelize.INTEGER, //Number of product in stock
      },
      buyPrice: {
        type: Sequelize.DECIMAL(10, 2),
      },
      MSRP: {
        type: Sequelize.DECIMAL(10, 2),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Products');
  },
};
