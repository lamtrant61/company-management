'use strict';

const { RoleCode } = require('../../constant/roles');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Roles',
      [
        {
          id: 1,
          code: RoleCode.PRESIDENT,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          code: RoleCode.MANAGER,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          code: RoleCode.LEADER,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          code: RoleCode.STAFF,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          code: RoleCode.CUSTOMER,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Roles', null, {});
  },
};
