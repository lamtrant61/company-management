'use strict';
const Employee = require('../models').Employee;
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Employees',
      [
        {
          employeeNumber: 1,
          firstName: 'John',
          lastName: 'Doe',
          extension: '1234',
          email: 'john@example.com',
          userId: 1,
          officeId: 1,
          reportsTo: null,
          jobTitle: 'President',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 2,
          firstName: 'Jane',
          lastName: 'Smith',
          extension: '5678',
          email: 'jane@example.com',
          userId: 2,
          officeId: 2,
          reportsTo: null,
          jobTitle: 'Manager',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 3,
          firstName: 'David',
          lastName: 'Brown',
          extension: '4321',
          email: 'david@example.com',
          userId: 3,
          officeId: 2,
          reportsTo: null,
          jobTitle: 'Leader',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 4,
          firstName: 'Sarah',
          lastName: 'Johnson',
          extension: '8765',
          email: 'sarah@example.com',
          userId: 4,
          officeId: 2,
          reportsTo: 2,
          jobTitle: 'Staff',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );

    const extistingEmployees = await Employee.count();

    // await queryInterface.sequelize.query(
    //   `ALTER SEQUENCE "Employees_employeeNumber_seq" RESTART WITH ${
    //     extistingEmployees + 1
    //   }`,
    // );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Employees', null, {});
  },
};
