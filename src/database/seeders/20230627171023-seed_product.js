'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Products',
      [
        {
          productCode: 'ff1100',
          productName: 'Iphone 14 pro max',
          productLine: 'Iphone',
          productScale: 'limited',
          productVendor: 'Apple',
          quantityInStock: 1000,
          buyPrice: 1400,
          MSRP: 1400,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          productCode: 'ff0000',
          productName: 'Samsung galaxy note 20 ultra',
          productLine: 'Samsung',
          productScale: 'limited',
          productVendor: 'Samsung',
          quantityInStock: 400,
          buyPrice: 2000,
          MSRP: 2000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Products', null, {});
  },
};
