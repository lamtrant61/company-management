'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Productlines',
      [
        {
          productLine: 'Iphone',
          textDescription: 'Apple store for all apple products',
          htmlDescription: 'Apple store for all apple products',
          image: 'iphone.png',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          productLine: 'Samsung',
          textDescription: 'Samsung store for all apple products',
          htmlDescription: 'Samsung store for all apple products',
          image: 'samsung.png',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          productLine: 'OPPO',
          textDescription: 'OPPO store for all apple products',
          htmlDescription: 'OPPO store for all apple products',
          image: 'oppo.png',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Productlines', null, {});
  },
};
