'use strict';

/** @type {import('sequelize-cli').Migration} */

// Password: test1!
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Users',
      [
        {
          username: 'president',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'president@gmail.com',
          roleId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'manager',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'manager@gmail.com',
          roleId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'leader',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'leader@gmail.com',
          roleId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'staff',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'staff@gmail.com',
          roleId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'customer',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'customer@gmail.com',
          roleId: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'demo',
          password: '$2b$10$etoNmlfWDnPehpD/HhlE9OSGdPe3UtjwMwiuxGQHejlgTHfVtcKa.',
          email: 'demo@gmail.com',
          roleId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  },
};
