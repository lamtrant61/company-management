'use strict';
const Customer = require('../models').Customer;
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Customers',
      [
        {
          userId: 5,
          customerName: 'Ricardo',
          contactLastName: 'Naruto',
          contactFirstName: 'Uzumaki',
          phone: '+84 6666 888 222',
          addressLine1: 'Lead Village',
          addressLine2: null,
          city: 'New York',
          state: null,
          postalCode: '0FF11',
          country: 'USA',
          salesRepEmployeeNumber: 4,
          creditLimit: 100000000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );

    const extistingCustomers = await Customer.count();

    // await queryInterface.sequelize.query(
    //   `ALTER SEQUENCE "Customers_employeeNumber_seq" RESTART WITH ${
    //     extistingCustomers + 1
    //   }`,
    // );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Customers', null, {});
  },
};
