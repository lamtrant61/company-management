const OrderStatus = {
  PROCESS: 'In Process',
  DISPUTED: 'Disputed',
  RESOLVED: 'Resolved',
  CANCELLED: 'Cancelled',
  HOLD: 'On Hold',
  SHIPPED: 'Shipped',
};

const OrderRule = {
  PROCESS: ['On Hold'],
  DISPUTED: ['In Process'],
  RESOLVED: ['Disputed'],
  CANCELLED: ['Resolved', 'In Process', 'On Hold'],
  HOLD: ['In Process'],
  SHIPPED: ['In Process', 'Resolved', 'On Hold'],
};

module.exports = { OrderStatus, OrderRule };
