const { RoleCode } = require('./roles.js');

function generateRules(req) {
  return {
    [RoleCode.PRESIDENT]: {
      ['Employees']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Logger']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Customers']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Products']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Productlines']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Payments']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Offices']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Orders']: {
        read: [],
        update: [],
        delete: [],
      },
    },
    [RoleCode.MANAGER]: {
      ['Employees']: {
        read: [['officeId', 'eq', req.user?.employeeId?.officeId]],
        update: [['officeId', 'eq', req.user?.employeeId?.officeId]],
      },
      ['Customers']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Products']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Productlines']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Payments']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Orders']: {
        read: [],
        update: [],
        delete: [],
      },
    },
    [RoleCode.LEADER]: {
      ['Employees']: {
        read: [
          'or',
          ['id', 'eq', req.user?.employeeId?.id],
          ['reportsTo', 'eq', req.user?.employeeId?.id],
        ],
      },
      ['Customers']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Products']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Productlines']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Payments']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Orders']: {
        read: [],
        update: [],
        delete: [],
      },
    },
    [RoleCode.STAFF]: {
      ['Employees']: {
        read: [['id', 'eq', req.user?.employeeId?.id]],
        update: [['id', 'eq', req.user?.employeeId?.id]],
      },
      ['Customers']: {
        read: [],
      },
      ['Products']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Productlines']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Payments']: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
      ['Orders']: {
        read: [],
        update: [],
        delete: [],
      },
    },
    [RoleCode.CUSTOMER]: {
      ['Employees']: {},
      ['Customers']: {
        read: [['id', 'eq', req.user?.customerId?.id]],
        update: [['id', 'eq', req.user?.customerId?.id]],
      },
      ['Products']: {},
      ['Productlines']: {},
      ['Payments']: {
        read: [['customerId', 'eq', req.user?.customerId?.id]],
      },
      ['Orders']: {
        create: [],
        read: [['customerId', 'eq', req.user?.customerId?.id]],
      },
    },
  };
}

module.exports = { generateRules };
