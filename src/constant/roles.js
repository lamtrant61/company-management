const RoleCode = {
  PRESIDENT: 'PRESIDENT',
  MANAGER: 'MANAGER',
  LEADER: 'LEADER',
  STAFF: 'STAFF',
  CUSTOMER: 'CUSTOMER',
};

module.exports = { RoleCode };
