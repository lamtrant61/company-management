// --------------- Connect to MongoDB
require('dotenv').config();
const mongoose = require('mongoose');

// const db = {
//   name: process.env.MONGO_NAME || '',
//   host: process.env.MONGO_HOST || '',
//   port: process.env.MONGO_PORT || '',
//   user: process.env.MONGO_USER || '',
//   password: process.env.MONGO_USER_PWD || '',
// };

const dbURI = 'mongodb://127.0.0.1:27017/mydatabase'; // MongoDB connection URL
// const dbURI = `mongodb://${db.user}:${encodeURIComponent(db.password)}@${db.host}:${db.port}/${
//   db.name
// }`;

// const dbURI = `mongodb://${db.host}:${db.port}/${db.name}`;
mongoose
  .connect(dbURI)
  .then(() => {
    console.log('Connected to MongoDB!');
  })
  .catch(err => {
    console.error('Error connecting to MongoDB:', err);
  });
