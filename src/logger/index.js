const winston = require('winston');
const { createLogger, format, transports } = require('winston');
// const { Logger } = require('./logger.model');

function generateLogger(type) {
  const logger = createLogger({
    levels: winston.config.syslog.levels,
    level: 'info',
    format: format.combine(
      format.timestamp(), // Thêm thông tin thời gian
      format.simple(), // Định dạng mặc định khác
    ),
    transports: [new winston.transports.File({ filename: `logger/${type}.log` })],
  });
  const typeLevelIndex = winston.config.syslog.levels[type];
  return {
    log: (message, username) => {
      logger[type](username ? `${username} : ${message}` : message);
      const currentLevelIndex = winston.config.syslog.levels[logger.level];
      // if (currentLevelIndex >= typeLevelIndex) {
      //   const newLog = new Logger({
      //     level: type,
      //     user: username ? username : '',
      //     message: message,
      //   });
      //   newLog.save();
      // }
    },
    update: function (level) {
      logger.level = level;
    },
  };
}

const loggers = {
  error: generateLogger('error'),
  warning: generateLogger('warning'),
  notice: generateLogger('notice'),
  info: generateLogger('info'),
  debug: generateLogger('debug'),
  updateLogLevel: function (level) {
    for (const key in this) {
      try {
        this[key].update(level);
      } catch (err) {}
    }
  },
};

module.exports = loggers;
