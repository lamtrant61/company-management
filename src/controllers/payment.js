const { Payment } = require('@src/database/models');
const { ControllerFactory } = require('@src/factories/common.controller');

module.exports = new ControllerFactory(Payment);
