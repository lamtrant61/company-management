require('dotenv').config();
const { RoleCode } = require('@src/constant/roles');
const { Op } = require('sequelize');
const bcrypt = require('bcrypt');
const { User, Employee, Role, Customer } = require('@src/database/models');
const asyncHandler = require('@src/utils/asyncHandler');
const { SuccessResponse } = require('@src/core/ApiResponse');
const { AuthFailureError, BadRequestError, InternalError } = require('@src/core/ApiError');
const { getUserData } = require('@src/utils/getUserData');
const { createToken } = require('@src/utils/authUtils');
const SALT_ROUNDS = 10;

exports.signin = asyncHandler(async (req, res, next) => {
  const { username, email, password } = req.body;

  // const whereCondition = {
  //   [Op.or]: [{ email }],
  // };

  const user = await User.findOne({
    where: { email },
  });

  if (!user) throw new BadRequestError('User not registered');

  const match = await bcrypt.compare(password, user.password);
  if (!match) throw new AuthFailureError('Invalid credentials');

  const token = await createToken(user);
  const userData = await getUserData(user);
  new SuccessResponse('Login Successful', {
    user: userData,
    token,
  }).send(res);
});

exports.signup = asyncHandler(async (req, res, next) => {
  const { username, email, password, ...data } = req.body;

  const existingUser = await User.findOne({
    where: { email },
  });

  if (existingUser) throw new BadRequestError('User already registered');

  const hashedPassword = await bcrypt.hash(password, SALT_ROUNDS);
  const customerRole = await Role.findOne({
    where: { code: RoleCode.CUSTOMER },
  });
  const newUser = await User.create({
    username: username || null,
    email: email || null,
    password: hashedPassword,
    roleId: customerRole.id,
  });

  if (!newUser) throw new InternalError('An error occurred. Please try again later.');

  if (data.salesRepEmployeeNumber) {
    const employee = await Employee.findOne({
      where: { employeeNumber: data.salesRepEmployeeNumber },
    });

    if (!employee) throw new BadRequestError('Employee not found');
  }
  await Customer.create({
    customerName: data.customerName || username,
    salesRepEmployeeNumber: data.salesRepEmployeeNumber || null,
    userId: newUser.id,
  });

  const token = await createToken(newUser);
  const userData = await getUserData(newUser);

  new SuccessResponse('Signup Successful', {
    user: userData,
    token,
  }).send(res);
});
