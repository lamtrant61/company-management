const { Employee, User } = require('@src/database/models');
const { ControllerFactory } = require('@src/factories/common.controller');
const asyncHandler = require('@src/utils/asyncHandler');
const bcrypt = require('bcrypt');
const { BadRequestError } = require('@src/core/ApiError');
const { SuccessResponse } = require('@src/core/ApiResponse');
const { ForbiddenError } = require('@src/core/ApiError');

const employeeController = new ControllerFactory(Employee);
employeeController.createController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['create']) {
      return next(new ForbiddenError());
    }
    const data = req.body;
    const salt = 10;

    // Check if userId exists
    if (data.userId) {
      const checkUserId = await User.findOne({ where: { id: data.userId } });
      if (!checkUserId) {
        return next(new BadRequestError('Invalid data'));
      }

      // Check if userId exists and connects to employee
      const checkEmployeeId = await Employee.findOne({ where: { userId: data.userId } });
      // Connect userId to employee if fit data
      if (
        !checkEmployeeId &&
        checkUserId.username === data.username &&
        checkUserId.email === data.email
      ) {
        await Employee.create({
          lastName: data.lastName,
          firstName: data.firstName,
          extension: data.extension,
          email: data.email,
          userId: data.userId,
          officeId: data.officeId,
          reportsTo: data.reportsTo,
          jobTitle: data.jobTitle,
        });
        const roleCode = await checkUserId.getRole();
        return new SuccessResponse('Connect userId to employee success', {
          userId: data.userId,
          username: data.username,
          email: data.email,
          role: roleCode.id,
        }).send(res);
      } else {
        return next(new BadRequestError('Invalid data'));
      }
    } else {
      // If userId not exists, create new user and employee
      const checkUserName = await User.findOne({ where: { username: data.username } });
      const checkEmail = await User.findOne({ where: { email: data.email } });
      if (checkUserName) {
        return next(new BadRequestError('Username already exists'));
      }
      if (checkEmail) {
        return next(new BadRequestError('Email already exists'));
      }
      const passHashed = bcrypt.hashSync(data.password, salt);
      const newUser = await User.create({
        username: data.username,
        email: data.email,
        password: passHashed,
        roleId: data.roleId,
      });
      const userId = newUser.dataValues.id;
      const roleCode = await newUser.getRole();
      await Employee.create({
        lastName: data.lastName,
        firstName: data.firstName,
        extension: data.extension,
        email: data.email,
        userId: userId,
        officeId: data.officeId,
        reportsTo: data.reportsTo,
        jobTitle: data.jobTitle,
      });
      return new SuccessResponse('Create new employee success', {
        userId: userId,
        username: data.username,
        email: data.email,
        role: roleCode.id,
      }).send(res);
    }
  });
};

module.exports = employeeController;
