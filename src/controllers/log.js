const { Logger } = require('@src/logger/logger.model');
const asyncHandler = require('@src/utils/asyncHandler');
const { NotFoundError } = require('@src/core/ApiError');
const { ForbiddenError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const logger = require('@src/logger');

function getAllController() {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['create']) {
      return next(new ForbiddenError('Permission denied create'));
    }
    const data = await Logger.find({}).sort({ createdAt: -1 }).limit(1000);

    return new SuccessResponse('Create success', data).send(res);
  });
}

function deleteController() {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['delete']) {
      return next(new ForbiddenError('Permission denied create'));
    }
    const data = await Logger.deleteMany({});

    return new SuccessResponse('Create success', data).send(res);
  });
}

function updateLevelController() {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['update']) {
      return next(new ForbiddenError('Permission denied create'));
    }
    logger.updateLogLevel(req.body.level);

    return new SuccessMsgResponse('Create success').send(res);
  });
}

module.exports = {
  getAllController,
  deleteController,
  updateLevelController,
};
