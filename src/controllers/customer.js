const { Customer, User } = require('@src/database/models');
const { ControllerFactory } = require('@src/factories/common.controller');
const asyncHandler = require('@src/utils/asyncHandler');
const bcrypt = require('bcrypt');
const { BadRequestError } = require('@src/core/ApiError');
const { SuccessResponse } = require('@src/core/ApiResponse');
const { ForbiddenError } = require('@src/core/ApiError');

const customerController = new ControllerFactory(Customer);
customerController.createController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['create']) {
      return next(new ForbiddenError());
    }
    const data = req.body;
    const salt = 10;

    // Check if userId exists
    if (data.userId) {
      const checkUserId = await User.findOne({ where: { id: data.userId } });
      if (!checkUserId) {
        return next(new BadRequestError('Invalid data'));
      }

      // Check if userId exists and connects to Customer
      const checkCustomerId = await Customer.findOne({ where: { userId: data.userId } });
      // Connect userId to Customer if fit data
      if (
        !checkCustomerId &&
        checkUserId.username === data.username &&
        checkUserId.email === data.email
      ) {
        await Customer.create({
          customerName: data.customerName,
          userId: data.userId,
          contactLastName: data.contactLastName,
          contactFirstName: data.contactFirstName,
          phone: data.phone,
          addressLine1: data.addressLine1,
          addressLine2: data.addressLine2,
          city: data.city,
          state: data.state,
          postalCode: data.postalCode,
          country: data.country,
          salesRepEmployeeNumber: data.salesRepEmployeeNumber,
          creditLimit: data.creditLimit,
        });
        const roleCode = await checkUserId.getRole();
        return new SuccessResponse('Connect userId to Customer success', {
          userId: data.userId,
          username: data.username,
          email: data.email,
          role: roleCode.id,
        }).send(res);
      } else {
        return next(new BadRequestError('Invalid data'));
      }
    } else {
      // If userId not exists, create new user and Customer
      const checkUserName = await User.findOne({ where: { username: data.username } });
      const checkEmail = await User.findOne({ where: { email: data.email } });
      if (checkUserName) {
        return next(new BadRequestError('Username already exists'));
      }
      if (checkEmail) {
        return next(new BadRequestError('Email already exists'));
      }
      const passHashed = bcrypt.hashSync(data.password, salt);
      const newUser = await User.create({
        username: data.username,
        email: data.email,
        password: passHashed,
        roleId: data.roleId || 5,
      });
      const userId = newUser.dataValues.id;
      const roleCode = await newUser.getRole();
      await Customer.create({
        customerName: data.customerName,
        userId: userId,
        contactLastName: data.contactLastName,
        contactFirstName: data.contactFirstName,
        phone: data.phone,
        addressLine1: data.addressLine1,
        addressLine2: data.addressLine2,
        city: data.city,
        state: data.state,
        postalCode: data.postalCode,
        country: data.country,
        salesRepEmployeeNumber: data.salesRepEmployeeNumber,
        creditLimit: data.creditLimit,
      });
      return new SuccessResponse('Create new Customer success', {
        userId: userId,
        username: data.username,
        email: data.email,
        role: roleCode.id,
      }).send(res);
    }
  });
};

module.exports = customerController;
