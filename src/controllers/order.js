const { Order, Customer, Product, Orderdetail, Payment } = require('@src/database/models');
const { ControllerFactory } = require('@src/factories/common.controller');
const asyncHandler = require('@src/utils/asyncHandler');
const { Op } = require('sequelize');
const { OrderStatus } = require('@src/constant/status');
const handleOrderStatus = require('@src/utils/handleStatus');
const { BadRequestError, ForbiddenError, NotFoundError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const { generatePaginationCondition, getQuery } = require('@src/utils/handleQuery');
const logger = require('@src/logger');

const orderController = new ControllerFactory(Order);

orderController.getAllController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['read']) {
      logger.warning.log('Permission denied read', req.user.email);
      return next(new ForbiddenError('Permission denied read'));
    }
    let allOrders = null;
    const query = getQuery(req);
    const paginationCondition = generatePaginationCondition(query);
    allOrders = await Order.findAll({
      where: req.rule['read'] || {},
      include: [
        {
          model: Orderdetail,
        },
      ],
      ...paginationCondition,
    });
    logger.info.log('Get all orders', req.user.email);
    return new SuccessResponse('Success', allOrders).send(res);
  });
};

orderController.createController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['create']) {
      logger.warning.log('Permission denied create ', req.user.email);
      return next(new ForbiddenError("Permission denied create"));
    }
    const data = req.body;
    let orderDetails = null;
    let createOrder = null;
    let createOrderDetail = null;
    let customer = null;
    let product = null;

    customer = await Customer.findOne({ where: { userId: req.user.id } });
    const currentDate = new Date();

    createOrder = await Order.create({
      orderDate: new Date(),
      requiredDate: new Date(),
      shippedDate: currentDate.setDate(currentDate.getDate() + 5),
      status: OrderStatus.PROCESS,
      comments: data.comments,
      customerId: customer.dataValues.id,
    });
    orderDetails = [];
    let total = 0;
    for (let i = 0; i < data.orderDetails.length; i++) {
      product = await Product.findOne({ where: { productCode: data.orderDetails[i].productCode } });

      if (!product) {
        logger.warning.log('Product not found ', req.user.email);
        next(new NotFoundError('Product not found'));
      }
      createOrderDetail = await Orderdetail.create({
        orderId: createOrder.dataValues.id,
        productCode: product.dataValues.productCode,
        quantityOrdered: data.orderDetails[i].quantityOrdered,
        priceEach: product.dataValues.buyPrice,
        orderLineNumber: null,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      total += data.orderDetails[i].quantityOrdered * product.dataValues.buyPrice;
      orderDetails[i] = createOrderDetail.dataValues;
    }
    await Payment.create({
      customerId: customer.dataValues.id,
      orderId: createOrder.dataValues.id,
      checkNumber: null,
      paymentDate: new Date(),
      amount: total,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
    createOrder.dataValues.orderDetail = orderDetails;
    logger.info.log('Create order success ', req.user.email);
    return new SuccessResponse('Create order success', createOrder).send(res);
  });
};

orderController.updateByIdController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['update']) {
      logger.warning.log('Permission denied update order ', req.user.email);
      return next(new ForbiddenError('Permission denied update'));
    }
    const { id } = req.params;
    let allDetails = null;
    const updateFields = req.body;
    if (updateFields.status === OrderStatus.CANCELLED) {
      logger.warning.log('Permission denied update order ', req.user.email);
      return next(new BadRequestError('You have to delete method'));
    }
    const data = await Order.findOne({
      where: {
        [Op.and]: [
          {
            id,
          },
          req.rule['update'] || {},
        ],
      },
    });
    if (!data) {
      logger.warning.log('Order not found ', req.user.email);
      return next(new NotFoundError('Order not found'));
    }
    if (!handleOrderStatus(data.dataValues.status, updateFields.status)) {
      logger.warning.log('Invalid status update order ', req.user.email);
      return next(new BadRequestError('Invalid status update'));
    }
    for (const key in req.body) {
      data[key] = req.body[key];
    }
    await data.save();
    if (updateFields.status === OrderStatus.SHIPPED) {
      allDetails = await Orderdetail.findAll({ 
        where: { orderId: id },
      });
      allDetails.forEach(async item => {
        let { quantityOrdered, productCode } = item.dataValues;
        let product = await Product.findOne({ where: { productCode } });
        product['quantityInStock'] -= quantityOrdered;
        await product.save();
      });
    }
    logger.info.log('Update success order ', req.user.email);
    return new SuccessResponse('Update success', data).send(res);
  });
};

orderController.deleteByIdController = function () {
  return asyncHandler(async (req, res, next) => {
    req.rule ? 0 : (req.rule = {});
    if (!req.rule['delete']) {
      logger.warning.log('Permission denied delete order ', req.user.email);
      return next(new ForbiddenError('Permission denied delete'));
    }
    const { id } = req.params;
    const data = await Order.findOne({
      where: {
        [Op.and]: [
          {
            id,
          },
          req.rule['delete'] || {},
        ],
      },
    });
    if (!data) {
      logger.warning.log('Order not found ', req.user.email);
      return next(new NotFoundError('Order not found'));
    }
    for (const key in req.query) {
      data[key] = req.query[key];
    }
    data['status'] = OrderStatus.CANCELLED;
    data['delete'] = true;
    await data.save();

    const allDetails = await Orderdetail.findAll({
      where: {
        orderId: id,
      },
    });
    allDetails.forEach(async item => {
      item['delete'] = true;
      await item.save();
    });
    const paymentDelete = await Payment.findOne({
      where: {
        orderId: id,
      },
    });
    paymentDelete['delete'] = true;
    await paymentDelete.save();
    logger.info.log('Delete success order ', req.user.email);
    return new SuccessMsgResponse('Delete success').send(res);
  });
};

module.exports = orderController;
