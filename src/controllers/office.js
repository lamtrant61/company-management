const Office = require('@src/database/models').Office;
const { ControllerFactory } = require('@src/factories/common.controller');

module.exports = new ControllerFactory(Office);
