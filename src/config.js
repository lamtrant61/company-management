require('dotenv').config();

const environment = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 3000;
const JWT_SECRET = process.env.JWT_SECRET;
const tokenInfo = {
  accessTokenValidity: parseInt(process.env.ACCESS_TOKEN_VALIDITY_SEC || '0'),
  issuer: process.env.TOKEN_ISSUER || '',
};
const logDirectory = process.env.LOG_DIR;

module.exports = {
  environment,
  port,
  JWT_SECRET,
  tokenInfo,
  logDirectory,
};
