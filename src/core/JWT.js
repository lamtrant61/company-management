require('dotenv').config();
const { promisify } = require('util');
const { sign, verify } = require('jsonwebtoken');
const { InternalError, TokenExpiredError, BadTokenError } = require('./ApiError');
const JWT_SECRET = process.env.JWT_SECRET;

class JwtPayLoad {
  constructor(issuer, subject, validity) {
    this.iss = issuer;
    this.sub = subject;
    this.iat = Math.floor(Date.now() / 1000);
    this.exp = this.iat + validity;
  }
}

async function encode(payload) {
  const cert = JWT_SECRET;
  if (!cert) throw new InternalError('Token generation failure');
  return promisify(sign)({ ...payload }, cert, { algorithm: 'HS256' });
}

async function validate(token) {
  const cert = JWT_SECRET;
  if (!cert) throw new InternalError('Token validation failure');

  try {
    return await promisify(verify)(token, cert);
  } catch (e) {
    // Logger.debug(e);
    if (e && e.name === 'TokenExpiredError') throw new TokenExpiredError();
    if (e && e.name === 'JsonWebTokenError') throw new BadTokenError();
    throw new BadTokenError();
  }
}

async function decode(token) {
  const cert = JWT_SECRET;
  try {
    return await promisify(verify)(token, cert, {
      ignoreExpiration: true,
    });
  } catch (e) {
    // Logger.debug(e);
    throw new BadTokenError();
  }
}

module.exports = { encode, decode, validate };
module.exports.JwtPayLoad = JwtPayLoad;
