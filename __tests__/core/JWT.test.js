const JWT = require('@src/core/JWT');
const { JwtPayLoad } = require('@src/core/JWT');
const { AuthFailureError, TokenExpiredError, BadTokenError } = require('@src/core/ApiError');

describe('JWT class tests', () => {
  const issuer = 'issuer';
  const subject = 'subject';
  const validity = 1;

  it('Should throw error for invalid token in JWT.decode', async () => {
    try {
      await JWT.decode('abc');
    } catch (e) {
      expect(e).toBeInstanceOf(BadTokenError);
    }

    // expect(readFileSpy).toBeCalledTimes(1);
  });

  it('Should generate a token for JWT.encode', async () => {
    const payload = new JwtPayLoad(issuer, subject, validity);
    const token = await JWT.encode(payload);

    expect(typeof token).toBe('string');
  });

  it('Should decode a valid token for JWT.decode', async () => {
    const payload = new JwtPayLoad(issuer, subject, validity);
    const token = await JWT.encode(payload);
    const decoded = await JWT.decode(token);

    expect(decoded).toMatchObject(payload);
  });

  it('Should parse an expired token for JWT.decode', async () => {
    const time = Math.floor(Date.now() / 1000);

    const payload = {
      iss: issuer,
      sub: subject,
      iat: time,
      exp: time,
    };
    const token = await JWT.encode(payload);
    const decoded = await JWT.decode(token);

    expect(decoded).toMatchObject(payload);
  });

  it('Should throw error for invalid token in JWT.validate', async () => {
    try {
      await JWT.validate('abc');
    } catch (e) {
      expect(e).toBeInstanceOf(BadTokenError);
    }
  });

  it('Should validate a valid token for JWT.validate', async () => {
    const payload = new JwtPayLoad(issuer, subject, validity);
    const token = await JWT.encode(payload);
    const decoded = await JWT.validate(token);

    expect(decoded).toMatchObject(payload);
  });

  it('Should validate a token expiry for JWT.validate', async () => {
    const time = Math.floor(Date.now() / 1000);

    const payload = {
      iss: issuer,
      sub: subject,
      iat: time,
      exp: time,
    };
    const token = await JWT.encode(payload);
    try {
      await JWT.validate(token);
    } catch (e) {
      expect(e).toBeInstanceOf(TokenExpiredError);
    }
  });
});
