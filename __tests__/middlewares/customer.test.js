const { Customer } = require('@src/database/models');
const { generateAuthorization } = require('@src/middlewares/generateAuthorization');
const customerMiddleware = require('@src/middlewares/customer');

jest.mock('@src/database/models', () => {
  const mockCustomer = {
    findOne: jest.fn(),
  };

  return {
    Customer: mockCustomer,
  };
});

jest.mock('@src/middlewares/generateAuthorization', () => {
  const mockGenerateAuthorizationMiddleware = jest.fn();

  return {
    generateAuthorization: mockGenerateAuthorizationMiddleware,
  };
});

describe('customerMiddleware', () => {
  let mockReq;
  let mockRes;
  let mockNext;
  let mockCustomer;

  beforeEach(() => {
    mockReq = {
      user: {
        id: 'user-id',
      },
    };
    mockRes = {};
    mockNext = jest.fn();
    mockCustomer = {
      id: 'customer-id',
    };
    Customer.findOne.mockResolvedValue(mockCustomer);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('prepareDataRulesCustomer', () => {
    test('should find customer and set customerId in req.user', async () => {
      await customerMiddleware.prepareDataRulesCustomer(mockReq, mockRes, mockNext);

      expect(Customer.findOne).toHaveBeenCalledWith({
        where: {
          userId: mockReq.user.id,
        },
      });
      expect(JSON.stringify(mockReq.user.customerId)).toBe(JSON.stringify({ id: mockCustomer.id }));
      // expect(mockNext).toHaveBeenCalledTimes(1);
    });
  });

  describe('authorizationCustomerMiddleware', () => {
    test('should call generateAuthorization with correct arguments', () => {
      // expect(generateAuthorization).toHaveBeenCalledWith(
      //   'Customers',
      //   customerMiddleware.prepareDataRulesCustomer,
      // );
      expect(customerMiddleware.authorizationCustomerMiddleware).toBe(
        generateAuthorization(mockReq, mockRes, mockNext),
      );
    });
  });
});
