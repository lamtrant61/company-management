const { Employee, Customer } = require('@src/database/models');
const { RoleCode } = require('@src/constant/roles');
const { generateAuthorization } = require('@src/middlewares/generateAuthorization');

const {
  prepareDataRulesPayment,
  authorizationPaymentMiddleware,
} = require('@src/middlewares/payment');

jest.mock('@src/database/models');

describe('generateAuthorization', () => {
  let req;

  beforeEach(() => {
    req = {
      user: {
        id: 1,
        role: RoleCode.ADMIN,
      },
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('prepareDataRulesPayment', () => {
    test('should set employeeId on req.user for non-customer roles', async () => {
      const employeeId = 123;
      Employee.findOne.mockResolvedValue({ id: employeeId });

      await prepareDataRulesPayment(req);

      expect(Employee.findOne).toHaveBeenCalledWith({
        where: {
          userId: req.user.id,
        },
      });
      expect(JSON.stringify(req.user.employeeId)).toBe(JSON.stringify({ id: employeeId }));
      expect(req.user.customerId).toBeUndefined();
    });

    test('should set customerId on req.user for customer role', async () => {
      const customerId = 456;
      Customer.findOne.mockResolvedValue({ id: customerId });

      req.user.role = RoleCode.CUSTOMER;

      await prepareDataRulesPayment(req);

      expect(Customer.findOne).toHaveBeenCalledWith({
        where: {
          userId: req.user.id,
        },
      });
      expect(req.user.employeeId).toBeUndefined();
      expect(JSON.stringify(req.user.customerId)).toBe(JSON.stringify({ id: customerId }));
    });
  });
});
