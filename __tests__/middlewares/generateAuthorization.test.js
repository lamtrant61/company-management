const { generateAuthorization } = require('@src/middlewares/generateAuthorization');
const { generateRules } = require('@src/constant/rules');
const { convertToBoolean, convertToWhereCondition } = require('@src/utils/handleRules');
const asyncHandler = require('@src/utils/asyncHandler');

jest.mock('@src/constant/rules');
jest.mock('@src/utils/handleRules');
jest.mock('@src/utils/asyncHandler');

describe('generateAuthorization', () => {
  let req;
  let res;
  let next;
  let prepareDataRulesMock;

  beforeEach(() => {
    req = {
      user: {
        role: 'admin',
      },
    };
    res = {};
    next = jest.fn();
    prepareDataRulesMock = jest.fn();
    generateRules.mockReturnValue({
      admin: {
        tableName: {
          create: true,
          read: true,
        },
      },
    });
    convertToBoolean.mockImplementation(value => value);
    convertToWhereCondition.mockImplementation(value => value);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should call prepareDataRules and set rule on req object', async () => {
    asyncHandler.mockImplementation(handler => handler);

    const middleware = generateAuthorization('tableName', prepareDataRulesMock);
    await middleware(req, res, next);

    expect(prepareDataRulesMock).toHaveBeenCalledWith(req);
    expect(generateRules).toHaveBeenCalledWith(req);
    expect(convertToBoolean).toHaveBeenCalledWith(true);
    expect(convertToWhereCondition).toHaveBeenCalledWith(true);

    expect(req.rule).toEqual({
      create: true,
      read: true,
    });

    expect(next).toHaveBeenCalled();
  });
});
