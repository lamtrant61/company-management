const authMiddleware = require('@src/middlewares/authentication');
const JWT = require('@src/core/JWT');
const { User } = require('@src/database/models');
const { getAccessToken, validateTokenData } = require('@src/utils/authUtils');
const { AuthFailureError, TokenExpiredError, NotFoundError } = require('@src/core/ApiError');

jest.mock('@src/core/JWT');
jest.mock('@src/database/models');
jest.mock('@src/utils/authUtils');
jest.mock('@src/core/ApiError');

describe('authMiddleware', () => {
  test('should set req.accessToken correctly', async () => {
    const req = {
      headers: {
        authorization: 'Bearer myAccessToken',
      },
    };

    const next = jest.fn();

    getAccessToken.mockReturnValue('myAccessToken');

    await authMiddleware(req, {}, next);

    expect(getAccessToken).toHaveBeenCalledWith(req.headers.authorization);
    expect(req.accessToken).toBe('myAccessToken');
  });

  test('should validate token and set req.user correctly', async () => {
    const req = {
      headers: {
        authorization: 'Bearer myAccessToken',
      },
    };

    const user = {
      id: 123,
      roleCode: 'admin',
    };

    const payload = { sub: user.id };

    JWT.validate.mockResolvedValue(payload);
    validateTokenData.mockReturnValue(undefined);
    User.findByPk.mockResolvedValue(user);

    const next = jest.fn();

    await authMiddleware(req, {}, next);

    expect(JWT.validate).toHaveBeenCalledWith(req.accessToken);
    expect(validateTokenData).toHaveBeenCalledWith(payload);
    expect(User.findByPk).toHaveBeenCalledWith(user.id);
  });

  test('should throw NotFoundError if user is not found', async () => {
    const req = {
      headers: {
        authorization: 'Bearer myAccessToken',
      },
    };

    User.findByPk.mockResolvedValue(null);

    const next = jest.fn();

    await authMiddleware(req, {}, next);
  });

  test('should throw TokenExpiredError if the token is expired', async () => {
    const req = {
      headers: {
        authorization: 'Bearer myAccessToken',
      },
    };

    const next = jest.fn();

    JWT.validate.mockRejectedValue(new TokenExpiredError());

    await authMiddleware(req, {}, next);
  });

  test('should re-throw any other error', async () => {
    const req = {
      headers: {
        authorization: 'Bearer myAccessToken',
      },
    };

    const next = jest.fn();
    const error = new Error('Some error');

    JWT.validate.mockRejectedValue(error);

    await authMiddleware(req, {}, next);
  });
});
