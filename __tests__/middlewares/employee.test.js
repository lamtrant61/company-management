const { Employee } = require('@src/database/models');
const { generateAuthorization } = require('@src/middlewares/generateAuthorization');

const employeeMiddleware = require('@src/middlewares/employee');

jest.mock('@src/database/models', () => {
  const mockEmployee = {
    findOne: jest.fn(),
  };

  return {
    Employee: mockEmployee,
  };
});

jest.mock('@src/middlewares/generateAuthorization', () => {
  const mockGenerateAuthorizationMiddleware = jest.fn();

  return {
    generateAuthorization: mockGenerateAuthorizationMiddleware,
  };
});

describe('employeeMiddleware', () => {
  let mockReq;
  let mockNext;
  let mockEmployee;

  beforeEach(() => {
    mockReq = {
      user: {
        id: 'user-id',
      },
    };
    mockNext = jest.fn();
    mockEmployee = {
      id: 'employee-id',
    };
    Employee.findOne.mockResolvedValue(mockEmployee);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should find employee and set employeeId in req.user', async () => {
    const middleware = employeeMiddleware.prepareDataRulesEmployee;

    await middleware(mockReq);

    expect(Employee.findOne).toHaveBeenCalledWith({
      where: {
        userId: mockReq.user.id,
      },
    });
    expect(JSON.stringify(mockReq.user.employeeId)).toBe(JSON.stringify({ id: mockEmployee.id }));
    // expect(mockNext).toHaveBeenCalledTimes(1);
  });

  test('should call generateAuthorization with correct arguments', () => {
    const middleware = employeeMiddleware.authorizationEmployeeMiddleware;

    // expect(generateAuthorization).toHaveBeenCalledWith(
    //   'Employees',
    //   employeeMiddleware.prepareDataRulesEmployee,
    // );
    // expect(middleware).toBe(generateAuthorization.mockReturnValue);
  });
});
