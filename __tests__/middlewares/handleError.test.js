const { NotFoundError, ApiError, InternalError, ErrorType } = require('@src/core/ApiError');
const logger = require('@src/logger');
const { handleError } = require('@src/middlewares/handleError');

jest.mock('@src/logger', () => ({
  error: {
    log: jest.fn(),
  },
}));

ApiError.handle = jest.fn();

describe('handleError', () => {
  let req, res, next;

  beforeEach(() => {
    req = { originalUrl: '/path', method: 'GET' };
    res = {
      status: jest
        .fn()
        .mockReturnValue({ json: jest.fn().mockReturnThis(), send: jest.fn().mockReturnThis() }),
      json: jest.fn(),
      send: jest.fn(),
    };
    next = jest.fn();
  });

  it('should handle ApiError and log internal errors', () => {
    const err = new ApiError('Error message', ErrorType.INTERNAL);

    handleError(err, req, res, next);

    expect(ApiError.handle).toHaveBeenCalled();
  });

  it('should handle ApiError and log non-internal errors', () => {
    const err = new ApiError('Error message', ErrorType.NOT_FOUND);

    handleError(err, req, res, next);

    expect(ApiError.handle).toHaveBeenCalled();
    expect(logger.error.log).not.toHaveBeenCalledWith(
      `500 - ${err.message} - ${req.originalUrl} - ${req.method}`,
    );
  });

  it('should handle non-ApiError and log the error', () => {
    const err = new Error('Internal server error');

    handleError(err, req, res, next);

    expect(logger.error.log).toHaveBeenCalledWith(
      `500 - ${err.message} - ${req.originalUrl} - ${req.method}`,
    );
    expect(logger.error.log).toHaveBeenCalledWith(err);
  });

  it('should send error response in development mode', () => {
    const err = new Error('Internal server error');
    process.env.NODE_ENV = 'development';

    handleError(err, req, res, next);

    expect(res.status).toHaveBeenCalledWith(500);
  });

  it('should handle non-ApiError and send InternalError response in non-development mode', () => {
    const err = new Error('Internal server error');
    process.env.NODE_ENV = 'production';

    const internalError = new InternalError();

    handleError(err, req, res, next);

    expect(ApiError.handle).toHaveBeenCalled();
    expect(res.status).not.toHaveBeenCalled();
    expect(res.send).not.toHaveBeenCalled();
  });
});
