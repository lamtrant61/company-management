const { ControllerFactory } = require('@src/factories/common.controller');
const { ForbiddenError, NotFoundError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const { Op } = require('sequelize');
const utils = require('@src/utils/handleQuery');

// Mock Model
const MockModel = {
  create: jest.fn(),
  findAll: jest.fn(),
  findOne: jest.fn(),
  destroy: jest.fn(),
};

// Mock req, res, next objects
const mockReq = {
  body: {},
  params: { id: '123' },
  rule: {},
};

const mockRes = {
  status: jest.fn(),
};

const mockNext = jest.fn();

describe('ControllerFactory', () => {
  let controllerFactory;

  beforeEach(() => {
    // Initialize ControllerFactory with MockModel
    controllerFactory = new ControllerFactory(MockModel);
  });

  afterEach(() => {
    // Reset all jest mocks after each test
    jest.clearAllMocks();
  });

  describe('createController', () => {
    it('should create a new object and send success response', async () => {
      // Mock req.rule['create']
      mockReq.rule['create'] = true;

      // Mock Model.create
      const createdObject = { id: '123', name: 'Test Object' };
      MockModel.create.mockResolvedValue(createdObject);

      // Call the createController method
      const createController = controllerFactory.createController();
      await createController(mockReq, mockRes, mockNext);

      // Assert that Model.create was called with req.body
      expect(MockModel.create).toHaveBeenCalledWith(mockReq.body);

      // Assert that SuccessResponse was sent with the created object
      // expect(mockRes.status).toHaveBeenCalledWith(200);
    });

    it("should send forbidden error if req.rule['create'] is falsy", async () => {
      // Mock req.rule['create']
      mockReq.rule['create'] = false;

      // Call the createController method
      const createController = controllerFactory.createController();
      await createController(mockReq, mockRes, mockNext);

      // Assert that ForbiddenError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
    });
  });

  describe('getAllController', () => {
    it('should get all data and send success response', async () => {
      // Mock req.rule['read']
      mockReq.rule['read'] = true;
      mockReq.query = {
        pages: 0,
        perpages: 10,
      };

      // Mock Model.findAll
      const allData = [{ id: '123', name: 'Test Object' }];
      MockModel.findAll.mockResolvedValue(allData);

      // Mock getQuery and generatePaginationCondition
      jest.spyOn(utils, 'getQuery').mockReturnValue({
        pages: 0,
        perpages: 10,
      });
      jest.spyOn(utils, 'generatePaginationCondition').mockReturnValue({
        offset: 0,
        limit: 10,
      });

      // Call the getAllController method
      const getAllController = controllerFactory.getAllController();
      await getAllController(mockReq, mockRes, mockNext);

      // Assert that req.rule['read'] was accessed
      expect(mockReq.rule['read']).toBe(true);

      // Assert that Model.findAll was called with the correct parameters
      expect(MockModel.findAll).toHaveBeenCalledWith(
        expect.objectContaining({
          where: mockReq.rule['read'] || {},
        }),
      );

      // Assert that SuccessResponse was sent with the retrieved data
      // expect(mockRes.status).toHaveBeenCalledWith(200);
    });

    it("should send forbidden error if req.rule['read'] is falsy", async () => {
      // Mock req.rule['read']
      mockReq.rule['read'] = false;

      // Call the getAllController method
      const getAllController = controllerFactory.getAllController();
      await getAllController(mockReq, mockRes, mockNext);

      // Assert that ForbiddenError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
    });
  });

  describe('getByIdController', () => {
    it('should get data by ID and send success response', async () => {
      // Mock req.rule['read']
      mockReq.rule['read'] = true;

      // Mock Model.findOne
      const data = { id: '123', name: 'Test Object' };
      MockModel.findOne.mockResolvedValue(data);

      // Call the getByIdController method
      const getByIdController = controllerFactory.getByIdController();
      await getByIdController(mockReq, mockRes, mockNext);

      // Assert that Model.findOne was called with the correct parameters
      expect(MockModel.findOne).toHaveBeenCalledWith({
        where: {
          [Op.and]: [{ id: mockReq.params.id }, mockReq.rule['read'] || {}],
        },
      });

      // Assert that SuccessResponse was sent with the retrieved data
      // expect(mockRes.status).toHaveBeenCalledWith(200);
    });

    it('should send not found error if data is not found', async () => {
      // Mock req.rule['read']
      mockReq.rule['read'] = true;

      // Mock Model.findOne returning null
      MockModel.findOne.mockResolvedValue(null);

      // Call the getByIdController method
      const getByIdController = controllerFactory.getByIdController();
      await getByIdController(mockReq, mockRes, mockNext);

      // Assert that NotFoundError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(NotFoundError));
    });

    it("should send forbidden error if req.rule['read'] is falsy", async () => {
      // Mock req.rule['read']
      mockReq.rule['read'] = false;

      // Call the getByIdController method
      const getByIdController = controllerFactory.getByIdController();
      await getByIdController(mockReq, mockRes, mockNext);

      // Assert that ForbiddenError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
    });
  });

  describe('updateByIdController', () => {
    it('should update data by ID and send success response', async () => {
      // Mock req.rule['update']
      mockReq.rule['update'] = {};
      mockReq.params = {
        id: '123',
      };
      mockReq.body = { id: '123', name: 'Test Object' };

      // Mock Model.findOne
      const data = { id: '123', name: 'Test Object', save: jest.fn() };
      MockModel.findOne.mockResolvedValue(data);

      // Call the updateByIdController method
      const updateByIdController = controllerFactory.updateByIdController();
      await updateByIdController(mockReq, mockRes, mockNext);

      // Assert that Model.findOne was called with the correct parameters
      expect(MockModel.findOne).toHaveBeenCalledWith({
        where: {
          [Op.and]: [{ id: mockReq.params.id }, mockReq.rule['update'] || {}],
        },
      });

      // Assert that data properties were updated with req.body
      expect(data).toEqual(expect.objectContaining(mockReq.body));

      // Assert that data.save was called
      expect(data.save).toHaveBeenCalled();

      // Assert that SuccessResponse was sent with the updated data
      // expect(mockRes.status).toHaveBeenCalledWith(200);
    });

    it('should send not found error if data is not found', async () => {
      // Mock req.rule['update']
      mockReq.rule['update'] = true;

      // Mock Model.findOne returning null
      MockModel.findOne.mockResolvedValue(null);

      // Call the updateByIdController method
      const updateByIdController = controllerFactory.updateByIdController();
      await updateByIdController(mockReq, mockRes, mockNext);

      // Assert that NotFoundError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(NotFoundError));
    });

    it("should send forbidden error if req.rule['update'] is falsy", async () => {
      // Mock req.rule['update']
      mockReq.rule['update'] = false;

      // Call the updateByIdController method
      const updateByIdController = controllerFactory.updateByIdController();
      await updateByIdController(mockReq, mockRes, mockNext);

      // Assert that ForbiddenError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
    });
  });

  describe('deleteByIdController', () => {
    it('should delete data by ID and send success response', async () => {
      // Mock req.rule['delete']
      mockReq.rule['delete'] = {};
      mockReq.params = { id: '123' };

      // Mock Model.findOne
      const data = { id: '123', name: 'Test Object', destroy: jest.fn() };
      MockModel.findOne.mockResolvedValue(data);

      // Mock Model.destroy
      MockModel.destroy.mockResolvedValue(1);

      // Call the deleteByIdController method
      const deleteByIdController = controllerFactory.deleteByIdController();
      await deleteByIdController(mockReq, mockRes, mockNext);

      // Assert that Model.findOne was called with the correct parameters
      expect(MockModel.findOne).toHaveBeenCalledWith({
        where: {
          [Op.and]: [{ id: mockReq.params.id }, mockReq.rule['delete'] || {}],
        },
      });

      // Assert that Model.destroy was called
      expect(data.destroy).toHaveBeenCalled();

      // Assert that SuccessMsgResponse was sent with success message
      // expect(mockRes.status).toHaveBeenCalledWith(200);
    });

    it('should send not found error if data is not found', async () => {
      // Mock req.rule['delete']
      mockReq.rule['delete'] = true;

      // Mock Model.findOne returning null
      MockModel.findOne.mockResolvedValue(null);

      // Call the deleteByIdController method
      const deleteByIdController = controllerFactory.deleteByIdController();
      await deleteByIdController(mockReq, mockRes, mockNext);

      // Assert that NotFoundError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(NotFoundError));
    });

    it("should send forbidden error if req.rule['delete'] is falsy", async () => {
      // Mock req.rule['delete']
      mockReq.rule['delete'] = false;

      // Call the deleteByIdController method
      const deleteByIdController = controllerFactory.deleteByIdController();
      await deleteByIdController(mockReq, mockRes, mockNext);

      // Assert that ForbiddenError was passed to next()
      // expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
    });
  });
});
