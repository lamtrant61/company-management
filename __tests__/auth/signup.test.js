const bcrypt = require('bcrypt');
const authUtils = require('@src/utils/authUtils');
const {User} = require('@src/database/models');
const app = require('@src/app');
const supertest = require('supertest');

const USER_NAME = 'random';
const USER_EMAIL = 'random@test.com';
const USER_PASSWORD = 'MyP@ssw0rd!';
const USER_PASSWORD_HASH = bcrypt.hashSync(USER_PASSWORD, 10);

const createTokensSpy = jest.spyOn(authUtils, 'createToken');

const bcryptCompareSpy = jest.spyOn(bcrypt, 'compare');

const mockUserFindByEmail = jest.fn(async (options) => {
  if (options.where.email === USER_EMAIL) {
    return Promise.resolve({
      id: 1,
      username: USER_NAME,
      email: USER_EMAIL,
      password: USER_PASSWORD_HASH,
      roleId: 5
    });
  }
  return Promise.resolve(null);
});

jest.spyOn(User, 'findOne').mockImplementation(mockUserFindByEmail);

jest.unmock('@src/utils/authUtils'); // remove any override made anywhere

function addHeaders(request) {
  return request.set('Content-Type', 'application/json').timeout(2000);
}

describe('Login basic route', () => {
  const endpoint = '/users/signin';
  const request = supertest(app);

  beforeEach(() => {
    mockUserFindByEmail.mockClear();
    bcryptCompareSpy.mockClear();
    createTokensSpy.mockClear();
  });

  it('Should send error when empty body is sent', async () => {
    const response = await addHeaders(request.post(endpoint));
    expect(response.status).toBe(400);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error when email is only sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({ email: USER_EMAIL }),
    );
    expect(response.status).toBe(400);
    expect(response.body.message).toMatch(/password/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error when password is only sent', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({ password: USER_PASSWORD }),
    );
    expect(response.status).toBe(400);
    expect(response.body.message).toMatch(/email/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error when email is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({ email: '123' }),
    );
    expect(response.status).toBe(400);
    expect(response.body.message).toMatch(/valid email/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error when password is not valid format', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: '123@abc.com',
        password: '123',
      }),
    );
    expect(response.status).toBe(400);
    expect(response.body.message).toMatch(/password length/);
    expect(response.body.message).toMatch(/6 char/);
    expect(mockUserFindByEmail).not.toBeCalled();
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error when user not registered for email', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: '123@abc.com',
        password: 'mypassword',
      }),
    );
    expect(response.status).toBe(400);
    expect(response.body.message).toMatch(/not registered/);
    expect(mockUserFindByEmail).toBeCalledTimes(1);
    expect(bcryptCompareSpy).not.toBeCalled();
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send error for wrong password', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        password: '1234567',
      }),
    );
    expect(response.status).toBe(401);
    expect(response.body.message).toMatch(/invalid credentials/i);
    expect(mockUserFindByEmail).toBeCalledTimes(1);
    expect(bcryptCompareSpy).toBeCalledTimes(1);
    expect(createTokensSpy).not.toBeCalled();
  });

  it('Should send success response for correct credentials', async () => {
    const response = await addHeaders(
      request.post(endpoint).send({
        email: USER_EMAIL,
        password: USER_PASSWORD,
      }),
    );
    expect(response.status).toBe(200);
    expect(response.body.message).toMatch(/Success/i);
    expect(response.body.data).toBeDefined();
    expect(response.body.data.user).toHaveProperty('id');
    expect(response.body.data.user).toHaveProperty('username');
    expect(response.body.data.user).toHaveProperty('email');
    expect(response.body.data.user).toHaveProperty('roleId');
    expect(response.body.data.token).toBeDefined();

    expect(mockUserFindByEmail).toBeCalledTimes(1);
    expect(bcryptCompareSpy).toBeCalledTimes(1);
    // expect(createTokensSpy).toBeCalledTimes(1);

    expect(bcryptCompareSpy).toBeCalledWith(USER_PASSWORD, USER_PASSWORD_HASH);
  });
});