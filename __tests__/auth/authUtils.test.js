require('dotenv').config();
const authUtils = require('@src/utils/authUtils');
const { AuthFailureError, InternalError } = require('@src/core/ApiError');
const JWT = require('@src/core/JWT');
const tokenInfo = {
  accessTokenValidity: parseInt(process.env.ACCESS_TOKEN_VALIDITY_SEC || '0'),
  issuer: process.env.TOKEN_ISSUER || '',
};

describe('authUtils', () => {
  describe('getAccessToken', () => {
    it('should throw an AuthFailureError if authorization is not provided', () => {
      expect(() => authUtils.getAccessToken()).toThrow(AuthFailureError);
    });

    it('should throw an AuthFailureError if authorization does not start with "Bearer "', () => {
      expect(() => authUtils.getAccessToken('Invalid Authorization')).toThrow(AuthFailureError);
    });

    it('should return the access token if authorization is valid', () => {
      const authorization = 'Bearer abc123';
      expect(authUtils.getAccessToken(authorization)).toBe('abc123');
    });
  });

  describe('validateTokenData', () => {
    it('should throw an AuthFailureError if payload is not provided', () => {
      expect(() => authUtils.validateTokenData()).toThrow(AuthFailureError);
    });

    it('should throw an AuthFailureError if payload is missing required fields', () => {
      const payload = { sub: '123' };
      expect(() => authUtils.validateTokenData(payload)).toThrow(AuthFailureError);
    });

    it('should throw an AuthFailureError if issuer is not valid', () => {
      const payload = { iss: 'invalid', sub: '123' };
      expect(() => authUtils.validateTokenData(payload)).toThrow(AuthFailureError);
    });

    it('should return true if payload is valid', () => {
      const payload = { iss: tokenInfo.issuer, sub: '123' };
      expect(authUtils.validateTokenData(payload)).toBe(true);
    });
  });

  describe('createToken', () => {
    it('should return a valid access token', async () => {
      const user = { id: '123' };
      const token = await authUtils.createToken(user);
      const payload = await JWT.decode(token);
      expect(payload.iss).toBe(tokenInfo.issuer);
      expect(payload.sub).toBe(user.id);
      expect(payload.exp).toBeGreaterThan(Date.now() / 1000);
    });
  });
});
