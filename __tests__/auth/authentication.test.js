require('dotenv').config();
const {  BadTokenError } = require('@src/core/ApiError');
const JWT = require('@src/core/JWT');
const app = require('@src/app');
const supertest = require('supertest');
const { User } = require('@src/database/models');
const authUtils = require('@src/utils/authUtils');
const tokenInfo = {
  accessTokenValidity: parseInt(process.env.ACCESS_TOKEN_VALIDITY_SEC || '0'),
  issuer: process.env.TOKEN_ISSUER || '',
};
const ACCESS_TOKEN = 'xyz';
const USER_ID = 3;
const getAccessTokenSpy = jest.spyOn(authUtils, 'getAccessToken');

const mockUserFindById = jest.fn(id => {
  if (id === USER_ID) {
    return {
      id: USER_ID,
      username: 'mockuser',
      email: 'mockuser@example.com',
      toString: () => USER_ID.toString(),
    };
  } else {
    return null;
  }
});

jest.spyOn(User, 'findByPk').mockImplementation(mockUserFindById);

const mockJwtValidate = jest.fn(async token => {
  if (token === ACCESS_TOKEN)
    return {
      iss: tokenInfo.issuer,
      sub: USER_ID.toString(),
      iat: 1,
      exp: 2,
    };
  throw new BadTokenError();
});
JWT.validate = mockJwtValidate;

function addHeaders(request) {
  return request.set('Content-Type', 'application/json').timeout(2000);
}

function addAuthHeaders(request, accessToken = ACCESS_TOKEN) {
  return request
    .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${accessToken}`)
    .timeout(2000);
}

describe('authentication validation', () => {
  const endpoint = '/customers/test';
  const request = supertest(app);

  beforeEach(() => {
    getAccessTokenSpy.mockClear();
    mockJwtValidate.mockClear();
    mockUserFindById.mockClear();
  });

  it('Should response with 401 if Authorization header is not passed', async () => {
    const response = await addHeaders(request.get(endpoint));
    expect(response.status).toBe(401);
    expect(response.body.message).toMatch(/Authorization/);
    expect(getAccessTokenSpy).not.toBeCalled();
  });

  it('Should response with 401 if Authorization header do not have Bearer', async () => {
    const response = await addHeaders(request.get(endpoint)).set(
      'Authorization',
      '123',
    );
    expect(response.status).toBe(401);
    expect(response.body.message).toMatch(/Authorization/);
    expect(getAccessTokenSpy).not.toBeCalled();
  });

  it('Should response with 401 if wrong Authorization header is provided', async () => {
    const response = await addHeaders(request.get(endpoint)).set(
      'Authorization',
      'Bearer 123',
    );
    expect(response.status).toBe(401);
    expect(response.body.message).toMatch(/token/i);
    // expect(getAccessTokenSpy).toBeCalledTimes(1);
    // expect(getAccessTokenSpy).toBeCalledWith('Bearer 123');
    // expect(getAccessTokenSpy).toReturnWith('123');
    expect(mockJwtValidate).toBeCalledTimes(1);
    expect(mockJwtValidate).toBeCalledWith('123');
    expect(mockUserFindById).not.toBeCalled();
  });

  it('Should response with 404 if correct Authorization header is provided', async () => {
    const response = await addAuthHeaders(request.get(endpoint));
    expect(response.body.message).toMatch(/not registered/);
    expect(response.body.message).not.toMatch(/token/i);
    expect(response.status).toBe(404);
    // expect(getAccessTokenSpy).toBeCalledTimes(1);
    // expect(getAccessTokenSpy).toBeCalledWith(`Bearer ${ACCESS_TOKEN}`);
    // expect(getAccessTokenSpy).toReturnWith(ACCESS_TOKEN);
    expect(mockJwtValidate).toBeCalledTimes(1);
    expect(mockJwtValidate).toBeCalledWith(ACCESS_TOKEN);
    expect(mockUserFindById).toBeCalledTimes(1);
  });
});
