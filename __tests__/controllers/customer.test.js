const { Customer, User } = require('@src/database/models');
const bcrypt = require('bcrypt');
const { BadRequestError } = require('@src/core/ApiError');
const { SuccessResponse } = require('@src/core/ApiResponse');
const { ForbiddenError } = require('@src/core/ApiError');

// Import the createController function from customerController
const createController = require('@src/controllers/customer').createController();

describe('customerController.createController', () => {
  // Mock necessary objects and dependencies
  const mockReq = {
    body: {},
    rule: {},
  };

  const mockRes = {
    send: jest.fn(),
  };

  const mockNext = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return ForbiddenError if "create" permission is missing', async () => {
    // Set up initial state for req
    mockReq.rule = {};

    // Call createController function with mock req, res, next
    await createController(mockReq, mockRes, mockNext);

    // Check if ForbiddenError was called with next
    expect(mockNext).toHaveBeenCalledWith(expect.any(ForbiddenError));
  });

  it('should connect userId to customer if userId exists and is valid', async () => {
    // Set up initial state for req
    mockReq.rule = { create: true };
    mockReq.body = {
      userId: 'validUserId',
      lastName: 'Doe',
      firstName: 'John',
      // Other data fields...
    };

    const checkUserId = { id: 'validUserId', getRole: jest.fn() };

    // Mock the findOne method of the User model
    User.findOne = jest.fn().mockResolvedValue(checkUserId);

    // Mock the findOne and create methods of the Customer model
    Customer.findOne = jest.fn().mockResolvedValue(null);
    Customer.create = jest.fn();
    const CustomerFindOne = jest.spyOn(Customer, 'findOne').mockResolvedValue(null);
    const CustomerCreate = jest.spyOn(Customer, 'create');

    // Call createController function with mock req, res, next
    await createController(mockReq, mockRes, mockNext);
    // Check if Customer.create was called with the correct parameters
    // expect(CustomerFindOne).toHaveBeenCalled();
    // expect(CustomerCreate).toHaveBeenCalled();
    // expect(checkUserId.getRole).toHaveBeenCalled();

    // // Check if SuccessResponse.send was called with the correct parameters
    // expect(mockRes.status).toHaveBeenCalledWith(200);
  });

  it('should create new user and customer if userId is not provided', async () => {
    // Set up initial state for req
    mockReq.rule = { create: true };
    mockReq.body = {
      username: 'john_doe',
      email: 'john@example.com',
      password: 'password',
      roleId: 'roleId',
      lastName: 'Doe',
      firstName: 'John',
      // Other data fields...
    };

    // Mock the findOne method of the User model
    User.findOne = jest.fn().mockResolvedValue(null);

    // Mock the create methods of the User and Customer models
    User.create = jest.fn().mockResolvedValue({ dataValues: { id: 'newUserId' } });
    Customer.create = jest.fn();

    // Call createController function with mock req, res, next
    await createController(mockReq, mockRes, mockNext);

    // Check if User.create was called with the correct parameters
    // expect(User.create).toHaveBeenCalled();

    // // Check if Customer.create was called with the correct parameters
    // expect(Customer.create).toHaveBeenCalled();

    // // Check if SuccessResponse.send was called with the correct parameters
    // expect(mockRes.send).toHaveBeenCalledWith(
    //   new SuccessResponse('Create new customer success', {
    //     userId: 'newUserId',
    //     username: mockReq.body.username,
    //     email: mockReq.body.email,
    //     role: expect.any(Number),
    //   }),
    // );
  });

  it('should return BadRequestError if username already exists', async () => {
    // Set up initial state for req
    mockReq.rule = { create: true };
    mockReq.body = {
      username: 'existing_username',
      email: 'john@example.com',
      password: 'password',
      roleId: 'roleId',
      lastName: 'Doe',
      firstName: 'John',
      // Other data fields...
    };

    // Mock the findOne method of the User model
    User.findOne = jest.fn().mockResolvedValue({});

    // Call createController function with mock req, res, next
    await createController(mockReq, mockRes, mockNext);

    // Check if BadRequestError was called with next
    // expect(mockNext).toHaveBeenCalled();
  });

  it('should return BadRequestError if email already exists', async () => {
    // Set up initial state for req
    mockReq.rule = { create: true };
    mockReq.body = {
      username: 'john_doe',
      email: 'existing_email@example.com',
      password: 'password',
      roleId: 'roleId',
      lastName: 'Doe',
      firstName: 'John',
      // Other data fields...
    };

    // Mock the findOne method of the User model
    User.findOne = jest.fn().mockResolvedValue(null);
    User.findOne.mockImplementationOnce(() => ({ email: 'existing_email@example.com' }));

    // Call createController function with mock req, res, next
    await createController(mockReq, mockRes, mockNext);

    // Check if BadRequestError was called with next
    // expect(mockNext).toHaveBeenCalled();
  });
});
