const { ControllerFactory } = require('@src/factories/common.controller');
const officeController = require('@src/controllers/office');

describe('Office Controller', () => {
  it('office is an instance of ControllerFactory', async () => {
    expect(officeController instanceof ControllerFactory).toBe(true);
  });
});
