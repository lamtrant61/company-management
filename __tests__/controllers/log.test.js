const { Logger } = require('@src/logger/logger.model');
const { NotFoundError, ForbiddenError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const {
  getAllController,
  deleteController,
  updateLevelController,
} = require('@src/controllers/log');

describe('getAllController', () => {
  it('should return data with success response if user has "create" rule', async () => {
    // Mock req, res, next objects
    const req = { rule: { create: true } };
    const res = { status: jest.fn() };
    const next = jest.fn();

    // Mock Logger.find() method
    Logger.find = jest.fn().mockResolvedValue([{ id: 1, message: 'Test message' }]);

    // Call the controller function
    await getAllController()(req, res, next);

    // Check the response
    // expect(res.status).toHaveBeenCalled();
  });

  it('should call next with ForbiddenError if user does not have "create" rule', async () => {
    // Mock req, res, next objects
    const req = { rule: {} };
    const res = {};
    const next = jest.fn();

    // Call the controller function
    await getAllController()(req, res, next);

    // Check the next function call
    expect(next).toHaveBeenCalledWith(expect.any(ForbiddenError));
  });
});

describe('deleteController', () => {
  it('should return data with success response if user has "delete" rule', async () => {
    // Mock req, res, next objects
    const req = { rule: { delete: true } };
    const res = { status: jest.fn() };
    const next = jest.fn();

    // Mock Logger.deleteMany() method
    Logger.deleteMany = jest.fn().mockResolvedValue({ n: 10, ok: 1 });

    // Call the controller function
    await deleteController()(req, res, next);

    // Check the response
    expect(res.status).toHaveBeenCalled();
  });

  it('should call next with ForbiddenError if user does not have "delete" rule', async () => {
    // Mock req, res, next objects
    const req = { rule: {} };
    const res = {};
    const next = jest.fn();

    // Call the controller function
    await deleteController()(req, res, next);

    // Check the next function call
    expect(next).toHaveBeenCalledWith(expect.any(ForbiddenError));
  });
});

describe('updateLevelController', () => {
  it('should update the log level and send a success response if the user has "update" permission', async () => {
    const req = { rule: { update: true }, body: { level: 'debug' } };
    const res = { status: jest.fn() };
    const next = jest.fn();

    const sendSpy = jest.fn();
    // SuccessMsgResponse.mockImplementation(() => ({
    //   send: sendSpy,
    // }));

    await updateLevelController()(req, res, next);

    // expect(logger.updateLogLevel).toHaveBeenCalledWith('debug');
    expect(res.status).toHaveBeenCalled();
  });

  it('should call next with ForbiddenError if the user does not have "update" permission', async () => {
    const req = { rule: {} };
    const res = {};
    const next = jest.fn();

    const nextErrorSpy = jest.spyOn(next, 'mockImplementation');

    await updateLevelController()(req, res, next);

    // expect(nextErrorSpy).toHaveBeenCalledWith(expect.any(ForbiddenError));
    // expect(nextErrorSpy.mock.calls[0][0].message).toBe('Permission denied create');
  });
});
