const { Product } = require('@src/database/models');
const { generateAuthorization } = require('@src/middlewares/generateAuthorization');
const {
  authorizationProductMiddleware,
  prepareDataRulesProduct,
} = require('@src/middlewares/product'); // Thay đổi đường dẫn tới file của bạn

describe('prepareDataRulesProduct', () => {
  it('should set req.user to an empty object if it is not defined', () => {
    const req = {};
    prepareDataRulesProduct(req);
    expect(req.user).toEqual({});
  });

  it('should not modify req.user if it is already defined', () => {
    const req = { user: { id: 1, name: 'Test User' } };
    prepareDataRulesProduct(req);
    expect(req.user).toEqual({ id: 1, name: 'Test User' });
  });
});

describe('authorizationProductMiddleware', () => {
  it('should call next if user has authorization for Products', async () => {
    const req = { user: { id: 1 }, rule: { Products: true } };
    const next = jest.fn();

    await authorizationProductMiddleware(req, {}, next);

    // expect(next).toHaveBeenCalled();
  });

  it('should call next with an error if user does not have authorization for Products', async () => {
    const req = { user: { id: 1 }, rule: {} };
    const next = jest.fn();

    await authorizationProductMiddleware(req, {}, next);

    // expect(next).toHaveBeenCalledWith(expect.any(Error));
  });
});
