const { Order, Customer, Product, Orderdetail, Payment } = require('@src/database/models');
const { ControllerFactory } = require('@src/factories/common.controller');
const asyncHandler = require('@src/utils/asyncHandler');
const { Op } = require('sequelize');
const { OrderStatus } = require('@src/constant/status');
const handleOrderStatus = require('@src/utils/handleStatus');
const { BadRequestError, ForbiddenError, NotFoundError } = require('@src/core/ApiError');
const { SuccessResponse, SuccessMsgResponse } = require('@src/core/ApiResponse');
const utils = require('@src/utils/handleQuery');
const orderController = require('@src/controllers/order');

describe('orderController.getAllController', () => {
  beforeEach(() => {
    req = {
      user: { email: "test@gmail.com",  },
      rule: { read: true },
    };
    req.query = {
      pages: 0,
      perpages: 10,
    };
    res = {
      status: jest.fn(),
    };
    next = jest.fn();
    sendMock = jest.spyOn(SuccessResponse.prototype, 'send');
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return all orders with valid permissions', async () => {
    // Mock the Order.findAll() function
    const mockOrderFindAll = jest.spyOn(Order, 'findAll').mockResolvedValue([]);
    jest.spyOn(utils, 'getQuery').mockReturnValue({
      pages: 0,
      perpages: 10,
    });
    jest.spyOn(utils, 'generatePaginationCondition').mockReturnValue({
      offset: 0,
      limit: 10,
    });
    const expectedResponse = new SuccessResponse('Success', []);
    const getAllOrders = orderController.getAllController();
    await getAllOrders(req, res, next);
    expect(mockOrderFindAll).toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(200);
    expect(next).not.toHaveBeenCalled();
  });

  it('should return a ForbiddenError with invalid permissions', async () => {
    const req = {
      user: { email: "test@gmail.com",  },
      rule: { read: false },
    };
    const res = {};
    const mockNext = jest.fn(error => {
      expect(error).toBeInstanceOf(ForbiddenError);
      expect(error.message).toBe('Permission denied read');
    });

    await orderController.getAllController()(req, res, mockNext);

    expect(mockNext).toHaveBeenCalled();
  });
});

describe('orderController.createController', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = {
      rule: { create: true },
      user: { email: "test@gmail.com",  id: 5 },
      body: {
        comments: 'Test order',
        orderDetails: [{ productCode: 'ff1100', quantityOrdered: 2 }],
      },
    };
    res = {
      status: jest.fn(),
    };
    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create an order with valid permissions and return success response', async () => {
    const mockCustomer = { dataValues: { id: 1 } };
    jest.spyOn(Customer, 'findOne').mockResolvedValue(mockCustomer);

    const mockOrder = jest.spyOn(Order, 'create').mockResolvedValue({
      dataValues: {
        id: 1,
      },
    });
    jest.spyOn(Product, 'findOne').mockResolvedValue({
      dataValues: {
        id: 1,
        productCode: 'ff1100',
        buyPrice: 100,
      },
    });
    jest.spyOn(Orderdetail, 'create').mockResolvedValue({
      dataValues: {
        id: 1,
      },
    });
    jest.spyOn(Payment, 'create').mockResolvedValue({});
    const createOrder = orderController.createController();
    await createOrder(req, res, next);

    expect(mockOrder).toHaveBeenCalled();
    expect(next).not.toHaveBeenCalled();
  });

  it('should return ForbiddenError when create permission is not granted', async () => {
    req.rule.create = false;
    const expectedError = new ForbiddenError("Permission denied create");

    const createOrder = orderController.createController();
    await createOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(expectedError);
    expect(res.status).not.toHaveBeenCalled();
  });

  it('should return not found product', async () => {
    const mockCustomer = { dataValues: { id: 1 } };
    jest.spyOn(Customer, 'findOne').mockResolvedValue(mockCustomer);

    const mockOrder = jest.spyOn(Order, 'create').mockResolvedValue({
      dataValues: {
        id: 1,
      },
    });
    jest.spyOn(Product, 'findOne').mockResolvedValue(false);
    const createOrder = orderController.createController();
    await createOrder(req, res, next);
    expect(res.status).not.toHaveBeenCalled();
  });
});

describe('orderController.updateByIdController', () => {
  let req, res, next;

  beforeEach(() => {
    req = {
      user: { email: "test@gmail.com",  },
      rule: { update: true },
      params: { id: '123' },
      body: { status: 'On Hold' },
    };

    res = {
      send: jest.fn(),
    };

    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should update an order with valid permissions and status', async () => {
    const orderData = {
      dataValues: {
        status: 'In Process',
      },

      save: jest.fn(),
    };

    jest.spyOn(Order, 'findOne').mockResolvedValue(orderData);

    const expectedResponse = new SuccessResponse('Update success', orderData);
    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);

    expect(Order.findOne).toHaveBeenCalledWith({
      where: {
        [Op.and]: [{ id: '123' }, req.rule['update'] || {}],
      },
    });

    expect(orderData.status).toBe('On Hold');
    expect(orderData.save).toHaveBeenCalled();
    expect(next).not.toHaveBeenCalled();
  });

  it('should update an order with valid permissions and status with status Shipped', async () => {
    req.body.status = 'Shipped';
    const orderData = {
      dataValues: {
        status: 'In Process',
      },

      save: jest.fn(),
    };
    const orderDetailData = [{
      dataValues: {
        quantityOrdered: 100,
        productCode: '123',
      },
    }];
    const productData = {
      dataValues: {
        quantityInStock: 100,
      },
      save: jest.fn(),
    };
    jest.spyOn(Order, 'findOne').mockResolvedValue(orderData);
    jest.spyOn(Orderdetail, 'findAll').mockResolvedValue(orderDetailData);
    jest.spyOn(Product, 'findOne').mockResolvedValue(productData);

    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);

    expect(Order.findOne).toHaveBeenCalledWith({
      where: {
        [Op.and]: [{ id: '123' }, req.rule['update'] || {}],
      },
    });

    expect(orderData.status).toBe('Shipped');
    expect(orderData.save).toHaveBeenCalled();
    expect(next).not.toHaveBeenCalled();
  });

  it('should handle forbidden error when update permission is denied', async () => {
    req.rule = {}; // No update permission

    const forbiddenError = new ForbiddenError('Permission denied update');
    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(forbiddenError);
    expect(res.send).not.toHaveBeenCalled();
  });

  it('should handle not found error when order is not found', async () => {
    jest.spyOn(Order, 'findOne').mockResolvedValue(null);

    const notFoundError = new NotFoundError('Order not found');
    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(notFoundError);
    expect(res.send).not.toHaveBeenCalled();
  });

  it('should use delete method to cancel order', async () => {
    req.body.status = "Cancelled"
    const badRequestError = new BadRequestError('You have to delete method');
    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(badRequestError);
    expect(res.send).not.toHaveBeenCalled();
  });

  it('should handle bad request error when status update is invalid', async () => {
    const orderData = {
      dataValues: {
        status: 'Shipped',
      },
    };

    jest.spyOn(Order, 'findOne').mockResolvedValue(orderData);

    const updateOrder = orderController.updateByIdController();

    await updateOrder(req, res, next);
    const badRequestError = new BadRequestError('Invalid status update');
    expect(next).toHaveBeenCalledWith(badRequestError);
    expect(res.send).not.toHaveBeenCalled();
  });
});


describe('orderController.deleteByIdController', () => {
  let req, res, next;

  beforeEach(() => {
    req = {
      user: { email: "test@gmail.com",  },
      rule: { delete: true },
      params: { id: '123' },
      query: { comments: 'demo comment' },
    };

    res = {
      send: jest.fn(),
    };

    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should delete an order with valid permissions', async () => {
    const orderData = {
      id: '123',
      save: jest.fn(),
    };

    jest.spyOn(Order, 'findOne').mockResolvedValue(orderData);
    jest.spyOn(Orderdetail, 'findAll').mockResolvedValue([{save: jest.fn()}]);
    jest.spyOn(Payment, 'findOne').mockResolvedValue({ save: jest.fn().mockResolvedValue() });

    const deleteOrder = orderController.deleteByIdController();

    await deleteOrder(req, res, next);

    expect(orderData.status).toBe(OrderStatus.CANCELLED);
    expect(orderData.delete).toBe(true);
    expect(orderData.save).toHaveBeenCalled();
    expect(next).not.toHaveBeenCalled();
  });

  it('should handle forbidden error when delete permission is denied', async () => {
    req.rule = {}; // No delete permission

    const forbiddenError = new ForbiddenError('Permission denied delete');
    const deleteOrder = orderController.deleteByIdController();

    await deleteOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(forbiddenError);
    expect(res.send).not.toHaveBeenCalled();
  });

  it('should handle not found error when order is not found', async () => {
    jest.spyOn(Order, 'findOne').mockResolvedValue(null);

    const notFoundError = new NotFoundError('Order not found');
    const deleteOrder = orderController.deleteByIdController();

    await deleteOrder(req, res, next);

    expect(next).toHaveBeenCalledWith(notFoundError);
    expect(res.send).not.toHaveBeenCalled();
  });
});