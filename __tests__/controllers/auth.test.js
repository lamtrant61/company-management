const { signup } = require('@src/controllers/auth.controller');
const { RoleCode } = require('@src/constant/roles');
const bcrypt = require('bcrypt');
const { User, Employee, Role, Customer } = require('@src/database/models');
const { SuccessResponse } = require('@src/core/ApiResponse');
const { AuthFailureError, BadRequestError, InternalError } = require('@src/core/ApiError');
const { getUserData } = require('@src/utils/getUserData');
const { createToken } = require('@src/utils/authUtils');
const asyncHandler = require('@src/utils/asyncHandler');
const { after } = require('lodash');
const user = require('@src/database/models/user');

jest.mock('@src/core/ApiResponse');

SuccessResponse.send = jest.fn();

describe('signup', () => {
    let createToken;
    let getUserData;
    let sendMock
    beforeEach(() => {
        Role.findOne = jest.fn();
        User.findOne = jest.fn();
        User.create = jest.fn();
        Employee.findOne = jest.fn();
        Customer.create = jest.fn();
        bcrypt.hash = jest.fn();
        createToken = jest.fn();
        getUserData = jest.fn();
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('should create a new user and customer, and send a success response', async () => {
      const req = {
        body: {
          username: 'john_doe',
          email: 'john@example.com',
          password: 'password123',
          customerName: 'John Doe Customer',
          salesRepEmployeeNumber: '123456',
        },
      };
  
      const res = {
        send: jest.fn(),
        status: jest.fn().mockResolvedValue({ json: jest.fn() }),
        json: jest.fn(),
      };
  
      const hashedPassword = 'hashedPassword';
      const roleId = 1;
      const newUser = {
        id: 1,
      };
      const token = 'token';
      const userData = {
        id: 1,
        username: 'john_doe',
        email: 'john@example.com',
        role: RoleCode.CUSTOMER,
      };
  
      bcrypt.hash.mockResolvedValue(hashedPassword);
      Role.findOne.mockResolvedValue({ id: roleId });
      User.findOne.mockResolvedValue(null);
      User.create.mockResolvedValue(newUser);
      Employee.findOne.mockResolvedValue({});
      Customer.create.mockResolvedValue({});
      createToken.mockResolvedValue(token);
      getUserData.mockResolvedValue(userData);
      await signup(req, res);
  
      expect(User.findOne).toHaveBeenCalled();
    });

  
  });
  