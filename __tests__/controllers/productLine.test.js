const { Product } = require('@src/database/models');
const { generateAuthorization } = require('@src/middlewares/generateAuthorization');
const {
  authorizationProductlineMiddleware,
  prepareDataRulesProductLine,
} = require('@src/middlewares/productline'); // Thay đổi đường dẫn tới file của bạn

describe('prepareDataRulesProductLine', () => {
  it('should set req.user to an empty object if it is not defined', () => {
    const req = {};
    prepareDataRulesProductLine(req);
    expect(req.user).toEqual({});
  });

  it('should not modify req.user if it is already defined', () => {
    const req = { user: { id: 1, name: 'Test User' } };
    prepareDataRulesProductLine(req);
    expect(req.user).toEqual({ id: 1, name: 'Test User' });
  });
});

describe('authorizationProductlineMiddleware', () => {
  it('should call next if user has authorization for Productlines', async () => {
    const req = { user: { id: 1 }, rule: { Productlines: true } };
    const next = jest.fn();

    await authorizationProductlineMiddleware(req, {}, next);

    // expect(next).toHaveBeenCalled();
  });

  it('should call next with an error if user does not have authorization for Productlines', async () => {
    const req = { user: { id: 1 }, rule: {} };
    const next = jest.fn();

    await authorizationProductlineMiddleware(req, {}, next);

    // expect(next).toHaveBeenCalledWith(expect.any(Error));
  });
});
