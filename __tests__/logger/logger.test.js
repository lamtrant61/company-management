const winston = require('winston');
const { createLogger, transports } = require('winston');
const { Logger } = require('@src/logger/logger.model');
const loggers = require('@src/logger'); // Thay đổi đường dẫn tới file của bạn

describe('generateLogger', () => {
  it('should create a logger with the specified type', () => {
    const logger = loggers.error;
    expect(logger).toBeDefined();
    expect(logger.log).toBeDefined();
    expect(logger.update).toBeDefined();
  });

  it('should log a message and create a new log entry if the logger level is equal to or higher than the specified type level', () => {
    // Mock Logger.save() method
    Logger.prototype.save = jest.fn();

    const logger = loggers.error;
    logger.log('Test message', 'Test user');

    // Check the log level and message
    // expect(Logger.prototype.save).not.toHaveBeenCalled();
  });

  it('should not create a new log entry if the logger level is lower than the specified type level', () => {
    // Mock Logger.save() method
    Logger.prototype.save = jest.fn();

    const logger = loggers.debug;
    logger.log('Test message', 'Test user');

    // Check the log level and message
    // expect(Logger.prototype.save).not.toHaveBeenCalled();
  });
});

describe('loggers', () => {
  it('should have loggers for each supported log level', () => {
    expect(loggers.error).toBeDefined();
    expect(loggers.warning).toBeDefined();
    expect(loggers.notice).toBeDefined();
    expect(loggers.info).toBeDefined();
    expect(loggers.debug).toBeDefined();
  });

  it('should update the log level of all loggers', () => {
    const updateLogLevelSpy = jest.spyOn(loggers, 'updateLogLevel');

    loggers.updateLogLevel('info');

    expect(updateLogLevelSpy).toHaveBeenCalledWith('info');
  });
});
