const { getQuery, generatePaginationCondition } = require('@src/utils/handleQuery');
require('dotenv').config();

describe('Test getQuery', () => {
  test('should return the correct result', () => {
    const query = {
      pages: '1',
      perpages: '2',
    };

    const result = getQuery({ query });

    expect(result).toEqual({
      pages: '1',
      perpages: '2',
    });
  });

  test('should not add missing parameters', () => {
    const query = {
      param1: 'value1',
    };

    const result = getQuery({ query });

    expect(result).toEqual({
      pages: 0,
      perpages: process.env.PERPAGE,
    });
  });
});

describe('Test generatePaginationCondition', () => {
  test('should return the correct result when pages and perpages are provided', () => {
    const pages = 2;
    const perpages = 10;

    const result = generatePaginationCondition({ pages, perpages });

    expect(result).toEqual({
      limit: perpages,
      offset: (pages - 1) * perpages,
    });
  });

  test('should return an empty object when pages or perpages are missing or invalid', () => {
    let result = generatePaginationCondition({ pages: 0, perpages: 10 });
    expect(result).toEqual({});

    result = generatePaginationCondition({ pages: 2, perpages: undefined });
    expect(result).toEqual({});

    result = generatePaginationCondition({ pages: null, perpages: 10 });
    expect(result).toEqual({});

    result = generatePaginationCondition({ pages: -1, perpages: 10 });
    expect(result).toEqual({});
  });
});
