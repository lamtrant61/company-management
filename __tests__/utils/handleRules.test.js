const {
  arrayToBoolean,
  arrayToWhereCondition,
  convertToBoolean,
  convertToRule,
  convertToWhereCondition,
  mergeBooleans,
  mergeConditions,
  standardizeConditions,
} = require('@src/utils/handleRules');
const { Op } = require('sequelize');

describe('Test mergeConditions', () => {
  test('should merge two WHERE conditions with "and" operator', () => {
    const condition1 = {
      field1: {
        [Op.eq]: 'value1',
      },
    };

    const condition2 = {
      field2: {
        [Op.gt]: 10,
      },
    };

    const operator = 'and';

    const result = mergeConditions(condition1, condition2, operator);

    expect(result).toEqual({
      [Op.and]: [condition1, condition2],
    });
  });

  test('should merge two WHERE conditions with "or" operator', () => {
    const condition1 = {
      field1: {
        [Op.eq]: 'value1',
      },
    };

    const condition2 = {
      field2: {
        [Op.gt]: 10,
      },
    };

    const operator = 'or';

    const result = mergeConditions(condition1, condition2, operator);

    expect(result).toEqual({
      [Op.or]: [condition1, condition2],
    });
  });
});

describe('Test arrayToWhereCondition', () => {
  test('should convert array to WHERE condition object', () => {
    const arr = ['field1', 'eq', 'value1'];

    const result = arrayToWhereCondition(arr);

    expect(result).toEqual({
      field1: {
        [Op.eq]: 'value1',
      },
    });
  });
});

describe('Test arrayToBoolean', () => {
  test('should convert array to boolean value based on the comparison operator', () => {
    expect(arrayToBoolean([5, 'eq', 5])).toBe(true);
    expect(arrayToBoolean([5, 'eq', 3])).toBe(false);
    expect(arrayToBoolean([5, 'gt', 3])).toBe(true);
    expect(arrayToBoolean([5, 'gt', 8])).toBe(false);
    expect(arrayToBoolean([5, 'gte', 3])).toBe(true);
    expect(arrayToBoolean([5, 'gte', 8])).toBe(false);
    expect(arrayToBoolean([5, 'ne', 3])).toBe(true);
    expect(arrayToBoolean([5, 'ne', 5])).toBe(false);
    expect(arrayToBoolean([null, 'is', null])).toBe(true);
    expect(arrayToBoolean([null, 'is', 'value'])).toBe(false);
    expect(arrayToBoolean([true, 'not', true])).toBe(false);
    expect(arrayToBoolean([true, 'not', false])).toBe(true);
    expect(arrayToBoolean([5, 'or', [1, 2, 3, 4, 5]])).toBe(true);
    expect(arrayToBoolean([5, 'or', [1, 2, 3, 4]])).toBe(false);
    expect(arrayToBoolean([5, 'lt', 8])).toBe(true);
    expect(arrayToBoolean([5, 'lt', 3])).toBe(false);
    expect(arrayToBoolean([5, 'lte', 8])).toBe(true);
    expect(arrayToBoolean([5, 'lte', 3])).toBe(false);
    expect(arrayToBoolean([5, 'between', [1, 10]])).toBe(true);
    expect(arrayToBoolean([5, 'between', [6, 10]])).toBe(false);
    expect(arrayToBoolean([5, 'notBetween', [1, 4]])).toBe(true);
    expect(arrayToBoolean([5, 'notBetween', [1, 6]])).toBe(false);
    expect(arrayToBoolean([5, 'in', [1, 2, 3, 4, 5]])).toBe(true);
    expect(arrayToBoolean([5, 'in', [1, 2, 3, 4]])).toBe(false);
    expect(arrayToBoolean([5, 'notIn', [1, 2, 3, 4]])).toBe(true);
    expect(arrayToBoolean([5, 'notIn', [1, 2, 3, 4, 5]])).toBe(false);
    expect(arrayToBoolean(['hello', 'like', 'he'])).toBe(true);
    expect(arrayToBoolean(['hello', 'like', 'lo'])).toBe(true);
    expect(arrayToBoolean(['hello', 'like', 'hi'])).toBe(false);
    expect(arrayToBoolean(['hello', 'notLike', 'he'])).toBe(false);
    expect(arrayToBoolean(['hello', 'notLike', 'lo'])).toBe(false);
    expect(arrayToBoolean(['hello', 'notLike', 'hi'])).toBe(true);
    expect(arrayToBoolean(['value', 'startsWith', 'val'])).toBe(true);
    expect(arrayToBoolean(['value', 'startsWith', 'abc'])).toBe(false);
    expect(arrayToBoolean(['value', 'endsWith', 'lue'])).toBe(true);
    expect(arrayToBoolean(['value', 'endsWith', 'abc'])).toBe(false);
    expect(arrayToBoolean(['value', 'substring', 'lue'])).toBe(true);
    expect(arrayToBoolean(['value', 'substring', 'abc'])).toBe(false);
    expect(arrayToBoolean(['hello', 'iLike', 'HE'])).toBe(true);
    expect(arrayToBoolean(['hello', 'iLike', 'HI'])).toBe(false);
    expect(arrayToBoolean(['hello', 'notILike', 'HE'])).toBe(false);
    expect(arrayToBoolean(['hello', 'notILike', 'HI'])).toBe(true);
    expect(arrayToBoolean(['value', 'any', ['abc', 'def', 'value']])).toBe(true);
    expect(arrayToBoolean(['value', 'any', ['abc', 'def']])).toBe(false);
  });
});

describe('Test mergeBooleans', () => {
  it('should merge two boolean values using the "or" operator', () => {
    const condition1 = true;
    const condition2 = false;
    const operator = 'or';
    const result = mergeBooleans(condition1, condition2, operator);
    expect(result).toBe(true);
  });

  it('should merge two boolean values using the "and" operator', () => {
    const condition1 = true;
    const condition2 = false;
    const operator = 'and';
    const result = mergeBooleans(condition1, condition2, operator);
    expect(result).toBe(false);
  });

  it('should return null if an invalid operator is provided', () => {
    const condition1 = true;
    const condition2 = false;
    const operator = 'invalid';
    const result = mergeBooleans(condition1, condition2, operator);
    expect(result).toBeNull();
  });
});

describe('convertToRule', () => {
  const conditions = [
    'and',
    ['field1', 'eq', 'value1'],
    'and',
    'or',
    ['field2', 'gt', 10],
    ['field3', 'startsWith', 'abc'],
    ['field4', 'in', [1, 2, 3]],
  ];

  it('should convert an array of conditions to a rule object', () => {
    const handleChangeCondition = arrayToWhereCondition;
    const handleMergeCondition = mergeConditions;
    const result = convertToRule(conditions, handleChangeCondition, handleMergeCondition);

    // Assert the resulting rule object
    expect(result).toEqual({
      [Op.and]: [
        {
          field1: {
            [Op.eq]: 'value1',
          },
        },
        {
          [Op.and]: [
            {
              [Op.or]: [
                {
                  field2: {
                    [Op.gt]: 10,
                  },
                },
                {
                  field3: {
                    [Op.startsWith]: 'abc',
                  },
                },
              ],
            },
            {
              field4: {
                [Op.in]: [1, 2, 3],
              },
            },
          ],
        },
      ],
    });
  });

  it('should return undefined if the input is invalid', () => {
    const handleChangeCondition = arrayToWhereCondition;
    const handleMergeCondition = mergeConditions;
    const result = convertToRule([], handleChangeCondition, handleMergeCondition);
    expect(JSON.stringify(result)).toEqual(JSON.stringify({}));
  });
});

describe('standardizeConditions', () => {
  const conditions = ['and', ['field1', 'eq', 'value1'], ['field2', 'gt', 10]];

  const data = {
    field1: 'standardizedValue1',
    field2: 'standardizedValue2',
  };

  it('should replace field values with corresponding data values', () => {
    const result = standardizeConditions(conditions, data);

    // Assert the resulting standardized conditions array
    expect(result).toEqual([
      'and',
      ['standardizedValue1', 'eq', 'value1'],
      ['standardizedValue2', 'gt', 10],
    ]);
  });
});

describe('convertToWhereCondition', () => {
  it('should convert array of conditions to SQL WHERE condition string', () => {
    const conditions = ['and', ['field1', 'eq', 'value1'], ['field2', 'gt', 10]];

    const result = convertToWhereCondition(conditions);

    // Assert the resulting SQL WHERE condition string
    expect(result).toEqual({
      [Op.and]: [{ field1: { [Op.eq]: 'value1' } }, { field2: { [Op.gt]: 10 } }],
    });
  });

  it('should return {} if input is []', () => {
    const invalidConditions = [];

    const result = convertToWhereCondition(invalidConditions);

    // Assert that the result is undefined
    expect(result).toEqual({});
  });
});

describe('convertToBoolean', () => {
  const conditions = ['and', ['field1', 'eq', 'value1'], ['field2', 'gt', 10]];

  const data = {
    field1: 'value1',
    field2: 11,
  };

  it('should convert array of conditions to boolean value based on field-value mappings', () => {
    const result = convertToBoolean(conditions, data);

    // Assert the resulting boolean value
    expect(result).toBe(true);
  });
});
